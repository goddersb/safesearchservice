﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace SafeSearchService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            try
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                new SafeSearchService()
                };
                ServiceBase.Run(ServicesToRun);
            }
            catch (Exception ex)
            {
                using (FileStream fileStream = new FileStream("C:\\Temp\\Logger.txt", FileMode.Create))
                {
                    using (BinaryWriter writer = new BinaryWriter(fileStream))
                    {
                        writer.Write($"{ex.Message}{ex.StackTrace.ToString()}{ex.InnerException}{ex.Data}");
                    }
                }

            }
        }
    }
}
