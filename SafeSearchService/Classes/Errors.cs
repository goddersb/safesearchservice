﻿namespace SafeSearchService.Classes
{
    class Errors
    {
        public static class Error_Types
        {
            public const string Validation = "Validation_Error";
            public const string Inactive = "Inactive_Account_Error";
            public const string Requests = "No_Requests_Error";
            public const string NoRecords = "No_Records_Found_Error";
        }
    }
}
