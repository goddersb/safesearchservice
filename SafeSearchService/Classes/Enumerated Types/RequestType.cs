﻿namespace SafeSearchService.Classes.Enumerated_Types
{
    public enum RequestType
    {
        Person = 0,
        Business = 1,
        Bank = 2
    }
}
