﻿using System.Threading.Tasks;
using Logger = NLog.Logger;
using System;
using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace SafeSearchService.Classes
{
    public class Excel
    {
        private static Logger _logger;
        private string _key { get; set; }
        private string _value { get; set; }

        public Excel(Logger logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Extract the values from the excel file using Open XML.
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<List<Dictionary<string, string>>> ExtractRequestValues(string filepath, string clientId)
        {
            List<Dictionary<string, string>> requestParams = new List<Dictionary<string, string>>();
            int sheetIndex = 0;

            try
            {
                using (SpreadsheetDocument spreadsheetdocument = SpreadsheetDocument.Open(filepath, true, new OpenSettings { RelationshipErrorHandlerFactory = _ => new RemoveMalformedHyperlinksRelationshipErrorHandler() }))
                {
                    WorkbookPart workbookPart = spreadsheetdocument.WorkbookPart;
                    var stringtable = workbookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();
                    IEnumerable<WorksheetPart> worksheetParts = workbookPart.WorksheetParts;
                    foreach (WorksheetPart worksheetPart in worksheetParts)
                    {
                        string worksheetName = workbookPart.Workbook.Descendants<Sheet>().ElementAt(sheetIndex).Name;
                        sheetIndex += 1;

                        Dictionary<string, string> requestParam = new Dictionary<string, string>();
                        IEnumerable<SheetData> sheetData = worksheetPart.Worksheet.Elements<SheetData>();
                        int RowCount = 0;
                        int CellCount = 0;
                        
                        // This is A2
                        int RowMin = 2;
                        int ColMin = 1;

                        //This is B50              
                        int RowMax = 50;
                        int ColMax = 2;

                        foreach (SheetData SD in sheetData)
                        {
                            foreach (Row row in SD.Elements<Row>().Where(r=>r.RowIndex < RowMax))
                            {
                                RowCount++; // We are in a new row
                                foreach (Cell cell in row.Elements<Cell>())
                                {
                                    CellCount++;
                                    if ((RowCount >= RowMin && CellCount >= ColMin) && (RowCount <= RowMax && CellCount <= ColMax))
                                    {
                                        if (cell.DataType == null && cell.CellValue != null)
                                        {
                                            // Check for pure numbers
                                            _value = cell.CellValue.Text;

                                            if (_key.Contains("Date"))
                                            {
                                                DateTime dateValue = DateTime.FromOADate(Convert.ToDouble(_value));
                                                _value = dateValue.ToString("yyyy-MM-dd");
                                            }

                                            //Add to the dictionary.
                                            if (!string.IsNullOrEmpty(_key))
                                            {
                                                requestParam.Add(_key, _value);
                                                _key = string.Empty;
                                            }
                                        }
                                        else if (cell.CellValue != null && cell.DataType.Value == CellValues.Boolean)
                                        {
                                            // Booleans
                                            _value = cell.CellValue.Text;

                                            //Add to the dictionary.
                                            if (!string.IsNullOrEmpty(_key))
                                            {
                                                requestParam.Add(_key, _value);
                                                _key = string.Empty;
                                            }
                                        }
                                        else if (cell.CellValue != null)
                                        {
                                            // A shared string
                                            if (stringtable != null)
                                            {
                                                // Cell value holds the shared string location
                                                if (CellCount==1)
                                                {
                                                    _key = stringtable.SharedStringTable.ElementAt(int.Parse(cell.CellValue.Text)).InnerText;
                                                }
                                                else
                                                {
                                                    _value = stringtable.SharedStringTable.ElementAt(int.Parse(cell.CellValue.Text)).InnerText;
                                                    if (_value == "TRUE")
                                                    {
                                                        _value = "1";
                                                    }
                                                    else if (_value == "FALSE")
                                                    {
                                                        _value = "0";
                                                    }

                                                    //Add to the dictionary.
                                                    if (!string.IsNullOrEmpty(_key))
                                                    {
                                                        requestParam.Add(_key, _value);
                                                        _key = string.Empty;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                CellCount = 0;
                            }
                        }

                        var stringParams = new StringBuilder();
                        foreach (KeyValuePair<string, string> keyValue in requestParam)
                        {
                            stringParams.Append($"Parameter ({keyValue.Key}), Value {keyValue.Value} |");
                        };

                        _logger.Info("Extracted the request parameters ({}) from the file ({}). ClientId={ClientId}", stringParams, filepath, clientId);

                        //Add the Sheet Name to the Dictionary object.
                        requestParam.Add("SheetName", worksheetName);

                        requestParams.Add(requestParam);

                    }
                }

                return requestParams;
            }
            catch (Exception ex)
            {   
                _logger.Error("Error extracting the values from the file ({}) | Error message ({}). ClientId={ClientId}", filepath, ex.Message, clientId);
                return requestParams;  //Ensure we return an empty List to ensure any errors do not stop other records being processed.

                //if (requestParams.Count> 1) //Ensure if processing multilple records we handle errors.
                //{
                //    return requestParams;
                //}
                //else
                //{
                //    return null;
                //}
            }
        }

        /// <summary>
        /// Required to handle email address in the XML.
        /// </summary>
        private sealed class RemoveMalformedHyperlinksRelationshipErrorHandler : RelationshipErrorHandler
        {
            public override string Rewrite(Uri partUri, string id, string uri) => $"https://error";
        }
    }
}