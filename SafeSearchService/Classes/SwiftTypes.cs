﻿namespace SafeSearchService.Classes
{
    public static class SwiftTypes
    {
        public const string MT101 = "MT101";
        public const string MT103 = "MT103";
        public const string MT192 = "MT192";
        public const string MT199 = "MT199";
        public const string MT200 = "MT200";
        public const string MT202 = "MT202";
        public const string MT210 = "MT210";
        public const string MT299 = "MT299";
        public const string MT380 = "MT380";
        public const string MT399 = "MT399";
        public const string MT499 = "MT499";
        public const string MT502 = "MT502";
        public const string MT540 = "MT540";
        public const string MT541 = "MT541";
        public const string MT542 = "MT542";
        public const string MT543 = "MT543";
        public const string MT545 = "MT545";
        public const string MT547 = "MT547";
        public const string MT548 = "MT548";
        public const string MT565 = "MT565";
        public const string MT599 = "MT599";
        public const string MT699 = "MT699";
        public const string MT799 = "MT799";
        public const string MT899 = "MT899";
        public const string MT920 = "MT920";
        public const string MT999 = "MT999";
    }
}