﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using SafeSearchService.Models;

namespace SafeSearchService.Classes
{
    public class Swift
    {
        private static Logger _logger;

        public Swift(Logger logger)
        {
            _logger = logger;
        }

        /// <summary>
        ///  Create the MT101 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT101 CreateMT101Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT101 mt101 = new MT101()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value,
                    SettleDate = swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Date").Value,
                    Amount = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Amount").Value),
                    Ccy = swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Currency").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Ref").Value,
                    OrderName = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Name").Value,
                    OrderAddress = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Address").Value,
                    OrderCity = swiftRequest.FirstOrDefault(pair => pair.Key == "Order City").Value,
                    OrderPostCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Order PostCode").Value,
                    OrderSwift = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Swift").Value,
                    OrderIban = swiftRequest.FirstOrDefault(pair => pair.Key == "Order IBAN").Value,
                    OrderOther = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Other").Value,
                    OrderVOCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Order VOCode").Value,
                    BeneficiaryName = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Name").Value,
                    BeneficiaryAddress1 = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Address1").Value,
                    BeneficiaryAddress2 = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Address2").Value,
                    BeneficiaryPostCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary PostCode").Value,
                    BeneficiarySwift = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Swift").Value,
                    BeneficiaryIBAN = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary IBAN").Value,
                    BeneficiaryBankName = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Bank Name").Value,
                    BeneficiaryBankAddress1 = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Bank Address1").Value,
                    BeneficiaryBankAddress2 = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Bank Address2").Value,
                    BeneficiaryBankPostCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Bank PostCode").Value,
                    BeneficiaryBankSwift = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Bank Swift").Value,
                    BeneficiaryBankIBAN = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Bank IBAN").Value,
                    BeneficiaryBankBIK = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Bank BIK").Value,
                    BeneficiaryBankINN = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Bank INN").Value,
                    BeneficiarySubBankName = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Sub Bank Name").Value,
                    BeneficiarySubBankAddress1 = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Sub Bank Address1").Value,
                    BeneficiarySubBankAddress2 = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Sub Bank Address2").Value,
                    BeneficiarySubBankPostCode = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Sub Bank PostCode").Value,
                    BeneficiarySubBankSwift = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Sub Bank Swift").Value,
                    BeneficiarySubBankIBAN = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Sub  BankIBAN").Value,
                    BeneficiarySubBankOther = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiarySub Bank Other").Value,
                    BeneficiarySubBankBik = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Sub Bank Bik").Value,
                    BeneficiarySubBankINN = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Sub Bank INN").Value,
                    SwiftUrgent = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Swift Urgent").Value)),
                    SwiftSendRef = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Swift SendRef").Value)),
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt101;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT101 payload, error message {}. ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }
        /// <summary>
        /// Create the MT103 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT103 CreateMT103Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT103 mt103 = new MT103()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value,
                    SettleDate = swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Date").Value,
                    Amount = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Amount").Value),
                    Ccy = swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Currency").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Ref").Value,
                    OrderName = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Name").Value,
                    OrderAddress = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Address").Value,
                    OrderCity = swiftRequest.FirstOrDefault(pair => pair.Key == "Order City").Value,
                    OrderPostCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Order PostCode").Value,
                    OrderSwift = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Swift").Value,
                    OrderIbanAccountNumber = swiftRequest.FirstOrDefault(pair => pair.Key == "Order IBAN/AccountNumber").Value,
                    OrderOther = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Other").Value,
                    OrderVOCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Order VOCode").Value,
                    BeneficiaryName = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Name").Value,
                    BeneficiaryAddress1 = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Address1").Value,
                    BeneficiaryAddress2 = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Address2").Value,
                    BeneficiaryPostCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary PostCode").Value,
                    BeneficiarySwift = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Swift").Value,
                    //BeneficiaryAccountNumber = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary IBAN/AccountNumber").Value,
                    BeneficiaryIBAN = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary IBAN/AccountNumber").Value,
                    BeneficiaryCcy = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Currency").Value,
                    BeneficiaryBankName = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary BankName").Value,
                    BeneficiaryBankAddress1 = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Bank Address1").Value,
                    BeneficiaryBankAddress2 = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Bank Address2").Value,
                    BeneficiaryBankPostCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Bank PostCode").Value,
                    BeneficiaryBankSwift = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Bank Swift").Value,
                    BeneficiaryBankIBAN = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Bank IBAN/Account Number").Value,
                    BeneficiarySubBankName = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Sub Bank Name").Value,
                    BeneficiarySubBankAddress1 = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Sub Bank Address1").Value,
                    BeneficiarySubBankAddress2 = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Sub Bank Address2").Value,
                    BeneficiarySubBankPostCode = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Sub Bank PostCode").Value,
                    BeneficiarySubBankSwift = swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Sub Bank Swift").Value,
                    BeneficiarySubBankIBAN = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Sub Bank IBAN/Account Number").Value,
                    //BeneficiarySubBankOther = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiarySubBankOther").Value,
                    BeneficiarySubBankBik = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "Beneficiary Sub Bank Bik").Value,
                    BeneficiarySubBankINN = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiarySubBankINN").Value,
                    SwiftUrgent = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "SwiftUrgent").Value)),
                    SwiftSendRef = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "SwiftSendRef").Value)),
                    ExchangeRate = 1,
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt103;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT103 payload, error message {}. ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT192 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT192 CreateMT192Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT192 mt192 = new MT192()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Ref").Value,
                    OrderRelatedRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Related Ref").Value,
                    OrderRelatedDate = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Related Date").Value,
                    OrderSwift = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value,
                    Message = swiftRequest.FirstOrDefault(pair => pair.Key == "Message").Value,
                    SwiftUrgent = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Swift Urgent").Value)),
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt192;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating  the MT192 payload, error message ({}). ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT199 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT199 CreateMT199Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT199 mt199 = new MT199()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value,
                    OrderSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Ref").Value,
                    OrderRelatedRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Related Ref").Value,
                    Message = swiftRequest.FirstOrDefault(pair => pair.Key == "Message").Value,
                    SwiftUrgent = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Swift Urgent").Value)),
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt199;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT199 payload, error message ({}). ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT200 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT200 CreateMT200Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT200 mt200 = new MT200()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "ClientSwiftCode").Value,
                    BrokerSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "BrokerSwiftCode").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "OrderRef").Value,
                    SettleDate = swiftRequest.FirstOrDefault(pair => pair.Key == "SettlementDate").Value,
                    Amount = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "SettlementAmount").Value),
                    Ccy = swiftRequest.FirstOrDefault(pair => pair.Key == "SettlementCurrency").Value,
                    BenIBAN = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiaryIBAN").Value,
                    OrderIBAN = swiftRequest.FirstOrDefault(pair => pair.Key == "OrderIBAN").Value,
                    OrderOther = swiftRequest.FirstOrDefault(pair => pair.Key == "OrderOther").Value,
                    SwiftUrgent = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "SwiftUrgent").Value)),
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt200;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT200 payload, error message {}. ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT202 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT202 CreateMT202Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT202 mt202 = new MT202()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "ClientSwiftCode").Value,
                    SettleDate = swiftRequest.FirstOrDefault(pair => pair.Key == "SettlementDate").Value,
                    Amount = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "SettlementAmount").Value),
                    Ccy = swiftRequest.FirstOrDefault(pair => pair.Key == "SettlementCurrency").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "OrderRef").Value,
                    OrderName = swiftRequest.FirstOrDefault(pair => pair.Key == "OrderName").Value,
                    OrderAddress = swiftRequest.FirstOrDefault(pair => pair.Key == "OrderAddress").Value,
                    OrderCity = swiftRequest.FirstOrDefault(pair => pair.Key == "OrderCity").Value,
                    OrderPostCode = swiftRequest.FirstOrDefault(pair => pair.Key == "OrderPostCode").Value,
                    OrderSwift = swiftRequest.FirstOrDefault(pair => pair.Key == "OrderSwift").Value,
                    OrderAccountNumber = swiftRequest.FirstOrDefault(pair => pair.Key == "OrderAccountNumber").Value,
                    OrderIban = swiftRequest.FirstOrDefault(pair => pair.Key == "OrderIban").Value,
                    OrderOther = swiftRequest.FirstOrDefault(pair => pair.Key == "OrderOther").Value,
                    OrderVOCode = swiftRequest.FirstOrDefault(pair => pair.Key == "OrderVOCode").Value,
                    BeneficiaryRef = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiaryRef").Value,
                    BeneficiaryName = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiaryName").Value,
                    BeneficiaryAddress1 = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiaryAddress1").Value,
                    BeneficiaryAddress2 = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiaryAddress2").Value,
                    BeneficiaryPostCode = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiaryPostCode").Value,
                    BeneficiarySwift = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiarySwift").Value,
                    BeneficiaryAccountNumber = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiaryAccountNumber").Value,
                    BeneficiaryIBAN = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiaryIBAN").Value,
                    BeneficiaryCcy = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiaryCurrency").Value,
                    BeneficiaryBankName = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiaryBankName").Value,
                    BeneficiaryBankAddress1 = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiaryBankAddress1").Value,
                    BeneficiaryBankAddress2 = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiaryBankAddress2").Value,
                    BeneficiaryBankPostCode = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiaryBankPostCode").Value,
                    BeneficiaryBankSwift = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiaryBankSwift").Value,
                    BeneficiaryBankIBAN = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiaryBankIBAN").Value,
                    BeneficiaryBankBIK = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiaryBankBIK").Value,
                    BeneficiaryBankINN = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiaryBankINN").Value,
                    BeneficiarySubBankName = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiarySubBankName").Value,
                    BeneficiarySubBankAddress1 = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiarySubBankAddress1").Value,
                    BeneficiarySubBankAddress2 = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiarySubBankAddress2").Value,
                    BeneficiarySubBankPostCode = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiarySubBankPostCode").Value,
                    BeneficiarySubBankSwift = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiarySubBankSwift").Value,
                    BeneficiarySubBankIBAN = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiarySubBankIBAN").Value,
                    BeneficiarySubBankOther = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiarySubBankOther").Value,
                    BeneficiarySubBankBik = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiarySubBankBik").Value,
                    BeneficiarySubBankINN = string.Empty, //swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiarySubBankINN").Value,
                    SwiftUrgent = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "SwiftUrgent").Value)),
                    SwiftSendRef = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "SwiftSendRef").Value)),
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt202;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT202 payload, error message {}. ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT210 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT210 CreateMT210Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT210 mt210 = new MT210()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "ClientSwiftCode").Value,
                    BenSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiarySwiftCode").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "OrderRef").Value,
                    RelatedOrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "RelatedOrderRef").Value,
                    SettleDate = swiftRequest.FirstOrDefault(pair => pair.Key == "SettlementDate").Value,
                    Amount = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "SettlementAmount").Value),
                    Ccy = swiftRequest.FirstOrDefault(pair => pair.Key == "SettlementCurrency").Value,
                    BrokerAccountNo = swiftRequest.FirstOrDefault(pair => pair.Key == "BrokerAccountNumber").Value,
                    OrderSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "OrderSwiftCode").Value,
                    BenSubBankSwift = swiftRequest.FirstOrDefault(pair => pair.Key == "BeneficiarySubBankSwift").Value,
                    SwiftUrgent = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "SwiftUrgent").Value)),
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt210;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT210 payload, error message {}. ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT299 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT299 CreateMT299Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT299 mt299 = new MT299()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value,
                    OrderSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Ref").Value,
                    OrderRelatedRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Related Ref").Value,
                    Message = swiftRequest.FirstOrDefault(pair => pair.Key == "Message").Value,
                    SwiftUrgent = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Swift Urgent").Value)),
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt299;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT299 payload, error message {}. ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT380 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT380 CreateMT380Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT380 mt380 = new MT380()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "ClientSwiftCode").Value,
                    BrokerSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "BrokerSwiftCode").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "OrderRef").Value,
                    PreviousOrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "PreviousOrderRef").Value,
                    LinkMessageType = swiftRequest.FirstOrDefault(pair => pair.Key == "LinkMessageType").Value,
                    TradeType = swiftRequest.FirstOrDefault(pair => pair.Key == "TradeType").Value,
                    SafeKeepingAccount = swiftRequest.FirstOrDefault(pair => pair.Key == "SafeKeepingAccount").Value,
                    BuySell = swiftRequest.FirstOrDefault(pair => pair.Key == "Buy/Sell").Value,
                    TradeDate = swiftRequest.FirstOrDefault(pair => pair.Key == "TradeDate").Value,
                    SettleDate = swiftRequest.FirstOrDefault(pair => pair.Key == "SettlementDate").Value,
                    Amount = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "SettlementAmount").Value),
                    OrderCcy = swiftRequest.FirstOrDefault(pair => pair.Key == "OrderCcy").Value,
                    CounterCcy = swiftRequest.FirstOrDefault(pair => pair.Key == "CounterCcy").Value,
                    Message = swiftRequest.FirstOrDefault(pair => pair.Key == "Message").Value,
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt380;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT380 payload, error message {}. ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT399 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT399 CreateMT399Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT399 mt399 = new MT399()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value,
                    OrderSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Swif tCode").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Ref").Value,
                    OrderRelatedRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Related Ref").Value,
                    Message = swiftRequest.FirstOrDefault(pair => pair.Key == "Message").Value,
                    SwiftUrgent = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Swift Urgent").Value)),
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt399;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT399 payload, error message {}. ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT499 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT499 CreateMT499Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT499 mt499 = new MT499()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value,
                    OrderSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Ref").Value,
                    OrderRelatedRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Related Ref").Value,
                    Message = swiftRequest.FirstOrDefault(pair => pair.Key == "Message").Value,
                    SwiftUrgent = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Swift Urgent").Value)),
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt499;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT499 payload, error message {}. ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT502 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT502 CreateMT502Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT502 mt502 = new MT502()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "ClientSwiftCode").Value,
                    BrokerSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "BrokerSwiftCode").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "OrderRef").Value,
                    BrokerAccountNumber = swiftRequest.FirstOrDefault(pair => pair.Key == "BrokerAccountNumber").Value,
                    TradeType = swiftRequest.FirstOrDefault(pair => pair.Key == "TradeType").Value,
                    CancelOrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "CancelOrderRef").Value,
                    BuySell = swiftRequest.FirstOrDefault(pair => pair.Key == "BuySell").Value,
                    Quantity = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "Quantity").Value),
                    Amount = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "Amount").Value),
                    Ccy = swiftRequest.FirstOrDefault(pair => pair.Key == "Ccy").Value,
                    InstrumentISIN = swiftRequest.FirstOrDefault(pair => pair.Key == "InstrumentISIN").Value,
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt502;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT502 payload, error message {}. ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT540 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT540 CreateMT540Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT540 mt540 = new MT540()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value,
                    BrokerSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Broker Swift Code").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Ref").Value,
                    BrokerAccountNumber = swiftRequest.FirstOrDefault(pair => pair.Key == "Broker Account Number").Value,
                    TradeType = swiftRequest.FirstOrDefault(pair => pair.Key == "Trade Type").Value,
                    CancelOrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Cancel Order Ref").Value,
                    IsEquity = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Is Equity").Value)),
                    TradeDate = swiftRequest.FirstOrDefault(pair => pair.Key == "Trade Date").Value,
                    SettleDate = swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Date").Value,
                    Amount = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "Amount").Value),
                    InstrumentISIN = swiftRequest.FirstOrDefault(pair => pair.Key == "Instrument ISIN").Value,
                    ClearerSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Clearer Swift Code").Value,
                    DeliveringAgentSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Delivering Agent Swift Code").Value,
                    DeliveringAgent = swiftRequest.FirstOrDefault(pair => pair.Key == "Delivering Agent").Value,
                    SettlementSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Swift Code").Value,
                    Instructions = swiftRequest.FirstOrDefault(pair => pair.Key == "Instructions").Value,
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt540;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT540 payload, error message {}. ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT541 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT541 CreateMT541Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT541 mt541 = new MT541()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "ClientSwiftCode").Value,
                    BrokerSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "BrokerSwiftCode").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "OrderRef").Value,
                    PreviousOrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "PreviousOrderRef").Value,
                    LinkedMessageType = swiftRequest.FirstOrDefault(pair => pair.Key == "LinkedMessageType").Value,
                    SwiftUrgent = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "SwiftUrgent").Value)),
                    TradeType = swiftRequest.FirstOrDefault(pair => pair.Key == "TradeType").Value,
                    PreparationDate = swiftRequest.FirstOrDefault(pair => pair.Key == "PreparationDate").Value,
                    TradeDate = swiftRequest.FirstOrDefault(pair => pair.Key == "TradeDate").Value,
                    SettlementDate = swiftRequest.FirstOrDefault(pair => pair.Key == "SettlementDate").Value,
                    DealPrice = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "DealPrice").Value),
                    DealPerc = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "DealPrice%").Value),
                    DealCcy = swiftRequest.FirstOrDefault(pair => pair.Key == "DealCurrency").Value,
                    InstrumentISIN = swiftRequest.FirstOrDefault(pair => pair.Key == "InstrumentISIN").Value,
                    Quantity = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "Quantity").Value),
                    SafeKeepingAccountType = swiftRequest.FirstOrDefault(pair => pair.Key == "SafeKeepingAccountType").Value,
                    SafeKeepingAccount = swiftRequest.FirstOrDefault(pair => pair.Key == "SafeKeepingAccount").Value,
                    SafeKeepingPlace = swiftRequest.FirstOrDefault(pair => pair.Key == "SafeKeepingPlace").Value,
                    CustodianSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "CustodianSwiftCode").Value,
                    OrderInstrumentType = swiftRequest.FirstOrDefault(pair => pair.Key == "OrderInstrumentType").Value,
                    SellerSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "SellerSwiftCode").Value,
                    SellerName = swiftRequest.FirstOrDefault(pair => pair.Key == "SellerName").Value,
                    //SellerAddress = swiftRequest.FirstOrDefault(pair => pair.Key == "SellerAddress").Value,
                    BuyerSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "BuyerSwiftCode").Value,
                    BuyerName = swiftRequest.FirstOrDefault(pair => pair.Key == "BuyerName").Value,
                    //BuyerAddress = swiftRequest.FirstOrDefault(pair => pair.Key == "BuyerAddress").Value,
                    SettlementSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "SettlementSwiftCode").Value,
                    SettlementTransactionType = swiftRequest.FirstOrDefault(pair => pair.Key == "SettlementTransactionType").Value,
                    SettlementTransactionIndicator = swiftRequest.FirstOrDefault(pair => pair.Key == "SettlementTransactionIndicator").Value,
                    SettlementPartyType = swiftRequest.FirstOrDefault(pair => pair.Key == "SettlementPartyType").Value,
                    SettlementPartyName = swiftRequest.FirstOrDefault(pair => pair.Key == "SettlementPartyName").Value,
                    DeliveryAgentCode = swiftRequest.FirstOrDefault(pair => pair.Key == "DeliveringAgentCode").Value,
                    SettlementAmountType = swiftRequest.FirstOrDefault(pair => pair.Key == "SettlementAmountType").Value,
                    SettlementAmount = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "SettlementAmount").Value),
                    SettlementCcy = swiftRequest.FirstOrDefault(pair => pair.Key == "SettlementAmountCurrency").Value,
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };
           
                return mt541;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT541 payload, error message {}. ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT542 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT542 CreateMT542Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT542 mt542 = new MT542()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value,
                    BrokerSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Broker Swift Code").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Ref").Value,
                    TradeType = swiftRequest.FirstOrDefault(pair => pair.Key == "Trade Type").Value,
                    CancelOrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Cancel Order Ref").Value,
                    IsEquity = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Is Equity").Value)),
                    TradeDate = swiftRequest.FirstOrDefault(pair => pair.Key == "TradeDate").Value,
                    SettlementDate = swiftRequest.FirstOrDefault(pair => pair.Key == "SettlementDate").Value,
                    SettlementAmount = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Amount").Value),
                    InstrumentISIN = swiftRequest.FirstOrDefault(pair => pair.Key == "Instrument ISIN").Value,
                    BrokerAccountNo = swiftRequest.FirstOrDefault(pair => pair.Key == "Broker Account Number").Value,
                    ClearerSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "ClearerSwiftCode").Value,
                    AgentAccountNo = swiftRequest.FirstOrDefault(pair => pair.Key == "Agent Account Number").Value,
                    SettlementSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Swift Code").Value,
                    Instructions =  swiftRequest.FirstOrDefault(pair => pair.Key == "Instructions").Value,
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt542;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT542 payload, error message ({}). ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT543 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT543 CreateMT543Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT543 mt543 = new MT543()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value,
                    BrokerSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Ref").Value,
                    PreviousOrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Previous Order Ref").Value,
                    LinkMessageType = swiftRequest.FirstOrDefault(pair => pair.Key == "Linked Message Type").Value,
                    SwiftUrgent = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Swift Urgent").Value)),
                    TradeType = swiftRequest.FirstOrDefault(pair => pair.Key == "Trade Type").Value,
                    PreparationDate = swiftRequest.FirstOrDefault(pair => pair.Key == "Preparation Date").Value,
                    TradeDate = swiftRequest.FirstOrDefault(pair => pair.Key == "Trade Date").Value,
                    SettlementDate = swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Date").Value,
                    DealPrice = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "Deal Price").Value),
                    DealPerc = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "Deal Price%").Value),
                    DealCcy = swiftRequest.FirstOrDefault(pair => pair.Key == "Deal Currency").Value,
                    InstrumentISIN = swiftRequest.FirstOrDefault(pair => pair.Key == "Instrument ISIN").Value,
                    LinkTypeReference = swiftRequest.FirstOrDefault(pair => pair.Key == "Link Type Reference").Value,
                    //LinkInstrumentQuantityType = swiftRequest.FirstOrDefault(pair => pair.Key == "LinkInstrumentQuantityType").Value,
                    //LinkInstrumentQuantity = Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "LinkInstrumentQuantity").Value),
                    //TradePlaceType = swiftRequest.FirstOrDefault(pair => pair.Key == "TradePlaceType").Value,
                    //TradePlaceCode = swiftRequest.FirstOrDefault(pair => pair.Key == "TradePlaceCode").Value,
                    SettlementCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Code").Value,
                    Quantity = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "Quantity").Value),
                    DenominationNarrative = swiftRequest.FirstOrDefault(pair => pair.Key == "Denomination Narrative").Value,
                    SafeKeepingAccountType = swiftRequest.FirstOrDefault(pair => pair.Key == "SafeKeeping Account Type").Value,
                    SafeKeepingAccount = swiftRequest.FirstOrDefault(pair => pair.Key == "SafeKeeping Account").Value,
                    SafeKeepingPlace = swiftRequest.FirstOrDefault(pair => pair.Key == "SafeKeeping Place").Value,
                    CustodianSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Custodian Swift Code").Value,
                    BuyerSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Buyer Swift Code").Value,
                    BuyerName = swiftRequest.FirstOrDefault(pair => pair.Key == "Buyer Name").Value,
                    //BuyerAddress = swiftRequest.FirstOrDefault(pair => pair.Key == "BuyerAddress").Value,
                    SellerSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Seller Swift Code").Value,
                    SellerName = swiftRequest.FirstOrDefault(pair => pair.Key == "Seller Name").Value,
                    //SellerAddress = swiftRequest.FirstOrDefault(pair => pair.Key == "SellerAddress").Value,
                    SettlementSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement SwiftCode").Value,
                    SettlementTransactionType = swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Transaction Type").Value,
                    SettlementTransactionIndicator = swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Transaction Indicator").Value,
                    AgentName = swiftRequest.FirstOrDefault(pair => pair.Key == "Agent Name").Value,
                    AgentCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Agent Code").Value,
                    SettlementPartyAccount = swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Party Account").Value,
                    SettlementAmountType = swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Amount Type").Value,
                    SettlementAmount = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Amount").Value),
                    SettlementCcy = swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Amount Currency").Value,
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt543;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT543 payload, error message {}. ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT545 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT545 CreateMT545Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT545 mt545 = new MT545()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value,
                    OrderSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Ref").Value,
                    RelatedRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Related Ref").Value,
                    TradeDate = swiftRequest.FirstOrDefault(pair => pair.Key == "Trade Date").Value,
                    SettleDate = swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Date").Value,
                    TradePrice = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "Trade Price").Value),
                    InstrumentISIN = swiftRequest.FirstOrDefault(pair => pair.Key == "Instrument ISIN").Value,
                    InstrumentDescription = swiftRequest.FirstOrDefault(pair => pair.Key == "Instrument Description").Value,
                    Quantity = Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Quantity").Value),
                    SettlementAmount = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Amount").Value),
                    Ccy = swiftRequest.FirstOrDefault(pair => pair.Key == "Ccy").Value,
                    SafekeepingAccount = swiftRequest.FirstOrDefault(pair => pair.Key == "Safekeeping Account Number").Value,
                    AgentAccountNumber = swiftRequest.FirstOrDefault(pair => pair.Key == "Agent Account Number").Value,
                    AgentSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Agent SwiftCode").Value,
                    CounterPartySwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Counterparty Swift Code").Value,
                    CounterPartySettlementSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Counterparty Settlement SwiftCode").Value,
                    SwiftUrgent = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Swift Urgent").Value)),
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt545;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT545 payload, error message {}. ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT547 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT547 CreateMT547Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT547 mt547 = new MT547()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value,
                    OrderSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Ref").Value,
                    RelatedRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Related Ref").Value,
                    TradeDate = swiftRequest.FirstOrDefault(pair => pair.Key == "Trade Date").Value,
                    SettleDate = swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Date").Value,
                    InstrumentISIN = swiftRequest.FirstOrDefault(pair => pair.Key == "Instrument ISIN").Value,
                    InstrumentDescription = swiftRequest.FirstOrDefault(pair => pair.Key == "Instrument Description").Value,
                    Quantity = Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Quantity").Value),
                    SettlementAmount = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Amount").Value),
                    Ccy = swiftRequest.FirstOrDefault(pair => pair.Key == "Ccy").Value,
                    SafekeepingAccount = swiftRequest.FirstOrDefault(pair => pair.Key == "Safekeeping Account Number").Value,
                    AgentAccountNumber = swiftRequest.FirstOrDefault(pair => pair.Key == "Agent Account Number").Value,
                    BrokerSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Broker Swift Code").Value,
                    CounterPartySwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Counter Party Swift Code").Value,
                    CounterPartySettlementSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Counter Party Settlement Swift Code").Value,
                    SwiftUrgent = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Swift Urgent").Value)),
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt547;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT547 payload, error message ({}). ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT548 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT548 CreateMT548Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT548 mt548 = new MT548()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value,
                    OrderSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Ref").Value,
                    RelatedRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Related Ref").Value,
                    TradeDate = swiftRequest.FirstOrDefault(pair => pair.Key == "Trade Date").Value,
                    SettleDate = swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Date").Value,
                    InstrumentISIN = swiftRequest.FirstOrDefault(pair => pair.Key == "Instrument ISIN").Value,
                    InstrumentDescription = swiftRequest.FirstOrDefault(pair => pair.Key == "Instrument Description").Value,
                    Ccy = swiftRequest.FirstOrDefault(pair => pair.Key == "Ccy").Value,
                    StatusCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Status Code").Value,
                    ReasonCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Reason Code").Value,
                    Quantity = Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Quantity").Value),
                    SettlementAmount = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "Settlement Amount").Value),
                    SafekeepingAccount = swiftRequest.FirstOrDefault(pair => pair.Key == "Safekeeping Account Number").Value,
                    AgentAccountNumber = swiftRequest.FirstOrDefault(pair => pair.Key == "Agent Account Number").Value,
                    AgentCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Agent Code").Value,
                    SwiftUrgent = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Swift Urgent").Value)),
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt548;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT548 payload, error message ({}). ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        public MT565 CreateMT565Payload(Dictionary<string,string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT565 mt565 = new MT565()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value,
                    OrderSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Ref").Value,
                    CorporateActionRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Corporate Action Ref").Value,
                    TradeType = swiftRequest.FirstOrDefault(pair => pair.Key == "Trade Type").Value,
                    CorporateActionEventIndicator = swiftRequest.FirstOrDefault(pair => pair.Key == "Corporate Action Event Indicator").Value,
                    RelatedOrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Related Order Ref").Value,
                    LinkIndicatorType = swiftRequest.FirstOrDefault(pair => pair.Key == "Link Indicator Type").Value,
                    LinkMessageType = swiftRequest.FirstOrDefault(pair => pair.Key == "Link Message Type").Value,
                    InstrumentISIN = swiftRequest.FirstOrDefault(pair => pair.Key == "Instrument ISIN").Value,
                    InstrumentQty = Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Instrument Qty").Value),
                    InstrumentQtyType = swiftRequest.FirstOrDefault(pair => pair.Key == "Instrument Qty Type").Value,
                    SafekeepingAccount = swiftRequest.FirstOrDefault(pair => pair.Key == "Safekeeping Account").Value,
                    BeneficiarySwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Benficiary Swift Code").Value,
                    PreviousActionNotification = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Previous Action Notification").Value)),
                    CorporateActionNumber = swiftRequest.FirstOrDefault(pair => pair.Key == "Corporate Action Number").Value,
                    CorporateActionIndicator = swiftRequest.FirstOrDefault(pair => pair.Key == "Corporate Action Indicator").Value,
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt565;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT565 payload, error message ({}). ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT599 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT599 CreateMT599Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT599 mt599 = new MT599()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value,
                    OrderSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Ref").Value,
                    OrderRelatedRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Related Ref").Value,
                    Message = swiftRequest.FirstOrDefault(pair => pair.Key == "Message").Value,
                    SwiftUrgent = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Swift Urgent").Value)),
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt599;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT599 payload, error message ({}). ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT699 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT699 CreateMT699Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT699 mt699 = new MT699()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value,
                    OrderSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Ref").Value,
                    OrderRelatedRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Related Ref").Value,
                    Message = swiftRequest.FirstOrDefault(pair => pair.Key == "Message").Value,
                    SwiftUrgent = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Swift Urgent").Value)),
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt699;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT699 payload, error message ({}). ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT799 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT799 CreateMT799Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT799 mt799 = new MT799()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value,
                    OrderSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Ref").Value,
                    OrderRelatedRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Related Ref").Value,
                    Message = swiftRequest.FirstOrDefault(pair => pair.Key == "Message").Value,
                    SwiftUrgent = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Swift Urgent").Value)),
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt799;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT799 payload, error message ({}). ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT899 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT899 CreateMT899Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT899 mt899 = new MT899()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value,
                    OrderSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Ref").Value,
                    OrderRelatedRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Related Ref").Value,
                    Message = swiftRequest.FirstOrDefault(pair => pair.Key == "Message").Value,
                    SwiftUrgent = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Swift Urgent").Value)),
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt899;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT899 payload, error message ({}). ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT920 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT920 CreateMT920Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT920 mt920 = new MT920()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value,
                    OrderSwift = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Ref").Value,
                    OrderCcy = swiftRequest.FirstOrDefault(pair => pair.Key == "Ccy").Value,
                    OrderAccountNumber = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Account Number").Value,
                    OrderAmount = Convert.ToDouble(swiftRequest.FirstOrDefault(pair => pair.Key == "Order Amount").Value),
                    OrderType = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Type").Value,
                    SwiftUrgent = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Swift Urgent").Value)),
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt920;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT920 payload, error message ({}). ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        /// <summary>
        /// Create the MT999 Payload for submission to the endpoint.
        /// </summary>
        /// <param name="swiftRequest"></param>
        /// <param name="clientId"></param>
        /// <param name="ftp"></param>
        /// <returns></returns>
        public MT999 CreateMT999Payload(Dictionary<string, string> swiftRequest, string clientId, bool ftp)
        {
            try
            {
                MT999 mt999 = new MT999()
                {
                    ClientId = Convert.ToInt32(clientId),
                    ClientSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value,
                    OrderSwiftCode = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value,
                    OrderRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Ref").Value,
                    OrderRelatedRef = swiftRequest.FirstOrDefault(pair => pair.Key == "Order Related Ref").Value,
                    Message = swiftRequest.FirstOrDefault(pair => pair.Key == "Message").Value,
                    SwiftUrgent = ReturnBoolValue(Convert.ToInt32(swiftRequest.FirstOrDefault(pair => pair.Key == "Swift Urgent").Value)),
                    Email = swiftRequest.FirstOrDefault(pair => pair.Key == "Email").Value,
                    Ftp = ftp
                };

                return mt999;
            }
            catch (Exception ex)
            {
                _logger.Error("Error creating the MT999 payload, error message ({}). ClientId={ClientId}", ex.Message, clientId);
                return null;
            }
        }

        private bool ReturnBoolValue(int value)
        {
            if (value == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
