﻿using Logger = NLog.Logger;
using SafeSearchService.Models;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace SafeSearchService.Classes
{
    public class SafeSearch
    {
        private static Logger _logger;

        public SafeSearch(Logger logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Call the Search endpoint in the SafeSearch API
        /// </summary>
        /// <param name="request"></param>
        /// <param name="clientDetails"></param>
        /// <param name="searchType"></param>
        /// <returns></returns>
        public async Task<Response> SubmitRequest(SearchRequest request, ClientDetails clientDetails, string searchType)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            Response response = new Response();
            int searchTypeInt;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("ApiKey", clientDetails.ApiKey.ToUpper());
                if (request != null)
                {
                    //Create a new instance of the Excel Class.
                    JSON json = new JSON();

                    //Serialise the Payload.
                    var payload = json.Serializer(request);

                    if(searchType=="Person")
                    {
                        searchTypeInt = 0;
                    }
                    else
                    {
                        searchTypeInt = 1;
                    }

                    //Construct the endpoint url.
                    string url = string.Concat(Properties.Settings.Default.SafeSearchUrl, searchTypeInt, "&ClientId=", Convert.ToInt32(clientDetails.ClientId));

                    try
                    {
                        StringContent requestContent = new StringContent(payload, Encoding.UTF8, "application/json");
                        responseMessage = await client.PostAsync(url, requestContent);
                        string responseBody = await responseMessage.Content.ReadAsStringAsync();
                        var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                        response.ResponseCode = responseCode;
                        response.Message = responseBody;
                        if (responseMessage.IsSuccessStatusCode)
                        {
                            _logger.Info("Request submitted to the endpoint ({}) with a reponse code of ({}) and message ({}). ClientId={ClientId}", url, responseCode, responseBody, clientDetails.ClientId);
                            return response;
                        }
                        else
                        {
                            _logger.Info("Request submitted to the endpoint ({}) with a reponse code of ({}) and message ({}). ClientId={ClientId}", url, responseCode, responseBody, clientDetails.ClientId);
                            return response;
                        }
                    }
                    catch (Exception ex)
                    {
                        response.Message = ex.Message;
                        response.ResponseCode = 0;

                        _logger.Error("Error sending the payload({}) to the Api endpoint ({}). ClientId={ClientId}", payload, url, clientDetails.ClientId);
                        return response;
                    }
                }
                else
                {
                    _logger.Error("No payload located within the request to the Api endpoint for a ({}) seacrh request. ClientId={ClientId}", searchType, clientDetails.ClientId);
                    return null;
                }
            }
        }

        /// <summary>
        /// Call the Swift Endpoint in the Swift API
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="swiftPayload"></param>
        /// <param name="clientDetails"></param>
        /// <param name="swiftType"></param>
        /// <returns></returns>
        public async Task<Response> SubmitSwiftRequest<T>(T swiftPayload, ClientDetails clientDetails, string swiftType, List<FilePaths> filePaths)
        {
            HttpResponseMessage responseMessage = new HttpResponseMessage();
            Response response = new Response();

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("ApiKey", clientDetails.ApiKey.ToUpper());
                if (swiftPayload != null)
                {
                    //Create a new instance of the Excel Class.
                    JSON json = new JSON();

                    //Serialise the Payload.
                    var payload = json.Serializer(swiftPayload);

                    //Construct the endpoint url.
                    string url = string.Concat(filePaths.Find(f => f.PathName == "SwiftApiUri").PathFilePath, swiftType, "?clientId=", Convert.ToInt32(clientDetails.ClientId));

                    try
                    {
                        StringContent requestContent = new StringContent(payload, Encoding.UTF8, "application/json");
                        responseMessage = await client.PostAsync(url, requestContent);
                        string responseBody = await responseMessage.Content.ReadAsStringAsync();
                        var responseCode = Convert.ToInt32(responseMessage.StatusCode);
                        response.ResponseCode = responseCode;
                        response.Message = responseBody;
                        if (responseMessage.IsSuccessStatusCode)
                        {
                            _logger.Info("Request submitted to the endpoint ({}) with a reponse code of ({}) and message ({}). ClientId={ClientId}", url, responseCode, responseBody, clientDetails.ClientId);
                            return response;
                        }
                        else
                        {
                            _logger.Info("Request submitted to the endpoint ({})  with a reponse code of ({}) and message ({}). ClientId={ClientId}", url, responseCode, responseBody, clientDetails.ClientId);
                            return response;
                        }
                    }
                    catch (Exception ex)
                    {
                        response.Message = ex.Message;
                        response.ResponseCode = 0;

                        _logger.Error("Error sending the payload({}) to the Api endpoint. ClientId={ClientId}", payload, clientDetails.ClientId);
                        return response;
                    }
                }
                else
                {
                    _logger.Error("No payload located within the request to the Api endpoint. ClientId={ClientId}", clientDetails.ClientId);
                    return null;
                }
            }
        }
    }
}