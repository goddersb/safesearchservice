﻿using NLog;
using SafeSearchService.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SafeSearchService.Classes
{
    public class GenericFunctions
    {
        private static Logger _logger;
        public GenericFunctions(Logger logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Perform validation checks on the IBAN values.
        /// </summary>
        /// <param name="ibanCode"></param>
        /// <returns></returns>
        public bool ValidIBAN(string ibanCode)
        {
            string varFirstLetter = ibanCode.Substring(0, 1).ToUpper();
            string varSecondLetter = ibanCode.Substring(1, 1).ToUpper();
            string JoinLetter = $"{varFirstLetter}{varSecondLetter}";

            if (ibanCode.Length < 15 || ibanCode.Length > 32)
            {
                return false;
            }
            else if (char.Parse(varFirstLetter) < 15 || char.Parse(varFirstLetter) > 90)
            {
                return false;
            }
            else if (char.Parse(varSecondLetter) < 15 || char.Parse(varSecondLetter) > 90)
            {
                return false;
            }
            else if (JoinLetter == "AX" && ibanCode.Length != 18)
            {
                return false;
            }
            else if (JoinLetter == "AL" && ibanCode.Length != 28)
            {
                return false;
            }
            else if (JoinLetter == "AD" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "AT" && ibanCode.Length != 20)
            {
                return false;
            }
            else if (JoinLetter == "BE" && ibanCode.Length != 16)
            {
                return false;
            }
            else if (JoinLetter == "BA" && ibanCode.Length != 20)
            {
                return false;
            }
            else if (JoinLetter == "BG" && ibanCode.Length != 22)
            {
                return false;
            }
            else if (JoinLetter == "HR" && ibanCode.Length != 21)
            {
                return false;
            }
            else if (JoinLetter == "CY" && ibanCode.Length != 28)
            {
                return false;
            }
            else if (JoinLetter == "CZ" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "DK" && ibanCode.Length != 18)
            {
                return false;
            }
            else if (JoinLetter == "EE" && ibanCode.Length != 20)
            {
                return false;
            }
            else if (JoinLetter == "FO" && ibanCode.Length != 18)
            {
                return false;
            }
            else if (JoinLetter == "FI" && ibanCode.Length != 18)
            {
                return false;
            }
            else if (JoinLetter == "FR" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "DE" && ibanCode.Length != 22)
            {
                return false;
            }
            else if (JoinLetter == "GI" && ibanCode.Length != 23)
            {
                return false;
            }
            else if (JoinLetter == "GR" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "GL" && ibanCode.Length != 18)
            {
                return false;
            }
            else if (JoinLetter == "HU" && ibanCode.Length != 28)
            {
                return false;
            }
            else if (JoinLetter == "IS" && ibanCode.Length != 26)
            {
                return false;
            }
            else if (JoinLetter == "IE" && ibanCode.Length != 22)
            {
                return false;
            }
            else if (JoinLetter == "IT" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "XK" && ibanCode.Length != 20)
            {
                return false;
            }
            else if (JoinLetter == "LV" && ibanCode.Length != 21)
            {
                return false;
            }
            else if (JoinLetter == "LI" && ibanCode.Length != 21)
            {
                return false;
            }
            else if (JoinLetter == "LT" && ibanCode.Length != 20)
            {
                return false;
            }
            else if (JoinLetter == "LU" && ibanCode.Length != 20)
            {
                return false;
            }
            else if (JoinLetter == "MK" && ibanCode.Length != 19)
            {
                return false;
            }
            else if (JoinLetter == "MT" && ibanCode.Length != 31)
            {
                return false;
            }
            else if (JoinLetter == "MD" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "MC" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "ME" && ibanCode.Length != 22)
            {
                return false;
            }
            else if (JoinLetter == "NL" && ibanCode.Length != 18)
            {
                return false;
            }
            else if (JoinLetter == "NO" && ibanCode.Length != 15)
            {
                return false;
            }
            else if (JoinLetter == "PL" && ibanCode.Length != 28)
            {
                return false;
            }
            else if (JoinLetter == "PT" && ibanCode.Length != 25)
            {
                return false;
            }
            else if (JoinLetter == "RO" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "SM" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "RS" && ibanCode.Length != 22)
            {
                return false;
            }
            else if (JoinLetter == "SK" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "SI" && ibanCode.Length != 19)
            {
                return false;
            }
            else if (JoinLetter == "ES" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "SE" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "CH" && ibanCode.Length != 21)
            {
                return false;
            }
            else if (JoinLetter == "GB" && ibanCode.Length != 22)
            {
                return false;
            }
            else if (JoinLetter == "AZ" && ibanCode.Length != 28)
            {
                return false;
            }
            else if (JoinLetter == "BH" && ibanCode.Length != 22)
            {
                return false;
            }
            else if (JoinLetter == "BR" && ibanCode.Length != 29)
            {
                return false;
            }
            else if (JoinLetter == "VG" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "CR" && ibanCode.Length != 21)
            {
                return false;
            }
            else if (JoinLetter == "DO" && ibanCode.Length != 28)
            {
                return false;
            }
            else if (JoinLetter == "GF" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "PF" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "TF" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "GE" && ibanCode.Length != 22)
            {
                return false;
            }
            else if (JoinLetter == "GP" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "GU" && ibanCode.Length != 28)
            {
                return false;
            }
            else if (JoinLetter == "IL" && ibanCode.Length != 23)
            {
                return false;
            }
            else if (JoinLetter == "JO" && ibanCode.Length != 30)
            {
                return false;
            }
            else if (JoinLetter == "KZ" && ibanCode.Length != 20)
            {
                return false;
            }
            else if (JoinLetter == "KW" && ibanCode.Length != 30)
            {
                return false;
            }
            else if (JoinLetter == "LB" && ibanCode.Length != 28)
            {
                return false;
            }
            else if (JoinLetter == "MQ" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "MR" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "MU" && ibanCode.Length != 30)
            {
                return false;
            }
            else if (JoinLetter == "YT" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "NC" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "PK" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "PS" && ibanCode.Length != 29)
            {
                return false;
            }
            else if (JoinLetter == "QA" && ibanCode.Length != 29)
            {
                return false;
            }
            else if (JoinLetter == "RE" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "BL" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "LC" && ibanCode.Length != 32)
            {
                return false;
            }
            else if (JoinLetter == "MF" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "PM" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "ST" && ibanCode.Length != 25)
            {
                return false;
            }
            else if (JoinLetter == "SA" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "TL" && ibanCode.Length != 23)
            {
                return false;
            }
            else if (JoinLetter == "TN" && ibanCode.Length != 24)
            {
                return false;
            }
            else if (JoinLetter == "TR" && ibanCode.Length != 26)
            {
                return false;
            }
            else if (JoinLetter == "UA" && ibanCode.Length != 29)
            {
                return false;
            }
            else if (JoinLetter == "AE" && ibanCode.Length != 23)
            {
                return false;
            }
            else if (JoinLetter == "WF" && ibanCode.Length != 27)
            {
                return false;
            }
            else if (JoinLetter == "ME" && ibanCode.Length != 22)
            {
                {
                    return false;
                }
            }
            else if (!ValidIBANConvertAndConfirm(ibanCode))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Perform validation checks on the Swift values.
        /// </summary>
        /// <param name="swiftCode"></param>
        /// <returns></returns>
        public  bool ValidSwift(string swiftCode)
        {
            swiftCode = swiftCode.Replace("-", "");
            if (swiftCode.Length != 8 && swiftCode.Length != 11 && swiftCode.Length != 6)
            {
                return false;
            }

            //if (!swiftCode.Take(6).All(char.IsLetter))
            //{
            //    return false;
            //}
            return true;
        }

        /// <summary>
        /// Validate IBAN value
        /// </summary>
        /// <param name="ibanCode"></param>
        /// <returns></returns>
        private static bool ValidIBANConvertAndConfirm(string ibanCode)
        {
            string tempIbanCode;
            string replaceChar;
            string replaceBy;
            long leftOver = 0;
            int digit;

            var holdingVal = ibanCode.Replace(" ", "").ToUpper();
            tempIbanCode = $"{holdingVal.Substring(4, holdingVal.Length - 4)}{holdingVal.Substring(0, 4)}";
            for (int i = 10; i <= 35; i++)
            {
                replaceChar = Convert.ToChar(i + 55).ToString();
                replaceBy = Convert.ToString(i);
                tempIbanCode = tempIbanCode.Replace(replaceChar, replaceBy);
            }

            for (int i = 0; i <= tempIbanCode.Length - 1; i++)
            {
                digit = Convert.ToInt32(tempIbanCode.Substring(i, 1));
                leftOver = (10 * leftOver + digit) % 97;
            }

            if (leftOver == 1)
            {
                return true;
            }
            return false;
        }

        //Clear out successfully processed files from the clients Extracts folder.
        public void ClearOutExtractFolder(Dictionary<string, string> filesProcessed)
        {
            foreach(KeyValuePair<string, string> fileProcessed in filesProcessed)
            {
                File.Delete(fileProcessed.Value);
            }
        }

        public bool IsNumericFromTryParse(string value)
        {
            double result = 0;
            return (double.TryParse(value, System.Globalization.NumberStyles.Float,
                    System.Globalization.NumberFormatInfo.CurrentInfo, out result));
        }
    }
}
