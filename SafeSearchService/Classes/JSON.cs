﻿using Newtonsoft.Json;

namespace SafeSearchService.Classes
{
    public class JSON 
    {
        public string Serializer(object payload)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.Formatting = Formatting.Indented;
            settings.NullValueHandling = NullValueHandling.Ignore;
            return JsonConvert.SerializeObject(payload, settings);
        }

        public string Deserializer(object payload)
        {
            return JsonConvert.DeserializeObject<dynamic>((string)payload);
        }

        public object Deserializer<T>(string responseBody)
        {
            return JsonConvert.DeserializeObject<T>(responseBody);
        }
    }
}
