﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace SafeSearchService.Classes
{
    public class Ftp
    {
        private static Logger _logger;

        public Ftp(Logger logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Return a list of all the client Id's currently available in the Ftp folder.
        /// </summary>
        /// <returns></returns>
        public List<string> GetAllFtpClients()
        {
            try
            {
                FtpWebRequest ftpRequest = (FtpWebRequest)WebRequest.Create(Properties.Settings.Default.FtpUrl);
                ftpRequest.Credentials = new NetworkCredential(Properties.Settings.Default.FtpUser, Properties.Settings.Default.FtpPassword);
                ftpRequest.Method = WebRequestMethods.Ftp.ListDirectory;
                FtpWebResponse response = (FtpWebResponse)ftpRequest.GetResponse();
                StreamReader streamReader = new StreamReader(response.GetResponseStream());

                List<string> directories = new List<string>();

                string line = streamReader.ReadLine();
                while (!string.IsNullOrEmpty(line))
                {
                    var lineArr = line.Split('/');
                    line = lineArr[lineArr.Count() - 1];
                    directories.Add(line);
                    line = streamReader.ReadLine();
                }

                streamReader.Close();

                return directories;
            }
            catch (Exception ex)
            {
                _logger.Error("Error ({})", ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Return a list of all files in the clients to_server folder.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public List<string> ListFtpClientFiles(string url, string clientId)
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(url);
                request.Method = WebRequestMethods.Ftp.ListDirectory;

                request.Credentials = new NetworkCredential(Properties.Settings.Default.FtpUser, Properties.Settings.Default.FtpPassword);
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);
                string names = reader.ReadToEnd();

                reader.Close();
                response.Close();

                return names.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
            catch (WebException ex)
            {
                _logger.Error("Error ({}). ClientId={ClientId}", ((FtpWebResponse)ex.Response).StatusDescription, clientId);
                return null;
            }

        }

        /// <summary>
        /// Extract the file from the Ftp folder for moving to the Extracats folder for the client.
        /// </summary>
        /// <param name="filesToServer"></param>
        /// <param name="clientId"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public string ExtractFtpClientFiles(string filesToServer, string clientId, string filePath, string clientPassword)
        {
            try
            {
                //Create the file and file path.
                var filename = $"{Path.GetFileName(filesToServer).Replace(".xlsm", "")}_{String.Format("{0:ddMMyyyy_HHmm}", DateTime.Now)}_FTP.xlsm";
                var filenameCorrected = string.Join("_", filename.Split(Path.GetInvalidFileNameChars()));
                var fileSaveLocation = string.Concat(filePath, Properties.Settings.Default.ExtractFolder, "\\", clientId, "\\", filenameCorrected);

                //Check to see if the directory exists and create if it does not.
                CheckCreateDirectory(Path.GetDirectoryName(fileSaveLocation));

                FtpWebRequest request =
                (FtpWebRequest)WebRequest.Create(filesToServer);
                request.Credentials = new NetworkCredential(Properties.Settings.Default.FtpUser, Properties.Settings.Default.FtpPassword);
                request.Method = WebRequestMethods.Ftp.DownloadFile;

                using (Stream ftpStream = request.GetResponse().GetResponseStream())
                using (Stream fileStream = File.Create(fileSaveLocation))
                {
                    ftpStream.CopyTo(fileStream);
                }

                //Unprotect the File.
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                process.StartInfo.FileName = "C:\\Gordons Work\\UnprotectFile\\UnprotectFile\\bin\\Debug\\netcoreapp3.1\\UnprotectFile.exe";
                process.StartInfo.Arguments = $"{fileSaveLocation} {clientPassword}";
                process.Start();
                var inProcess = true;
                while (inProcess)
                {
                    process.Refresh();
                    System.Threading.Thread.Sleep(10);
                    if (process.HasExited)
                    {
                        inProcess = false;
                    }
                }

                return fileSaveLocation;
            }
            catch (WebException ex)
            {
                _logger.Error("Error ({}). ClientId={ClientId}", ((FtpWebResponse)ex.Response).StatusDescription, clientId);
                return null;
            }
        }

        /// <summary>
        /// Copy the files to the clients to_client folder.
        /// </summary>
        /// <param name="fileMovePath"></param>
        /// <param name="filePath"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public bool CopyFileToFtp(string fileMovePath, string filePath, string clientId)
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(fileMovePath);
                request.Credentials = new NetworkCredential(Properties.Settings.Default.FtpUser, Properties.Settings.Default.FtpPassword);
                request.Method = WebRequestMethods.Ftp.UploadFile;

                using (Stream fileStream = File.OpenRead(filePath))
                using (Stream ftpStream = request.GetRequestStream())
                {
                    fileStream.CopyTo(ftpStream);
                }

                return true;
            }
            catch (WebException ex)
            {
                _logger.Error("Error ({}). ClientId={ClientId}", ((FtpWebResponse)ex.Response).StatusDescription, clientId);
                return false;
            }
        }

        /// <summary>
        /// Delete the file once processed in the clients to_server folder.
        /// </summary>
        /// <param name="fileToDelete"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public bool DeleteFtpFileFromFolder(string fileToDelete, string clientId)
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(fileToDelete);
                request.Method = WebRequestMethods.Ftp.DeleteFile;
                request.Credentials = new NetworkCredential(Properties.Settings.Default.FtpUser, Properties.Settings.Default.FtpPassword);
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                response.Close();

                return true;
            }
            catch(WebException ex)
            {
                _logger.Error("Error ({}) whilst deleting the file ({}). ClientId={ClientId}", ((FtpWebResponse)ex.Response).StatusDescription, Path.GetFileName(fileToDelete), clientId);
                return false;
            }
        }

        private void CheckCreateDirectory (string directoryName)
        {
            if (!Directory.Exists(directoryName))
            {
                Directory.CreateDirectory(directoryName);
            }
        }
    }
}
