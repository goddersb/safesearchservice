﻿using NLog;
using SafeSearchService.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace SafeSearchService.Classes
{
    public class SQL
    {
        private static Logger _logger;
        public SQL(Logger logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// Determine if the email has been processed previously.
        /// </summary>
        /// <param name="emailId"></param>
        /// <returns></returns>
        public async Task<bool> IsEmailProcessed(string emailId)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand comm = new SqlCommand(Properties.Settings.Default.IsEmailProcessed, conn))
                    {
                        comm.CommandType = CommandType.StoredProcedure;
                        comm.Parameters.Add("@EmailId", SqlDbType.VarChar, 250).Value = emailId;
                        SqlDataReader reader = await comm.ExecuteReaderAsync();
                        if (reader.HasRows)
                        {
                            return true;
                        }
                        return false;
                    }
                }
            }
            catch (SqlException ex)
            {
                _logger.Error("Error checking if the email Id ({}) has been processed. Message ({}).", emailId, ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Reset the table SafeSearchService_EmailDetails, this can arise when emails are processed but an issue arises.
        /// </summary>
        /// <returns></returns>
        public async Task<bool> ResetEmailDetails()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand comm = new SqlCommand(Properties.Settings.Default.ResetEmailDetails, conn))
                    {
                        comm.CommandType = CommandType.StoredProcedure;
                        await comm.ExecuteNonQueryAsync();
                    }
                }
                return true;
            }
            catch (SqlException ex)
            {
                _logger.Error("Error reseting the table SafeSearchService_EmailDetails. Message ({}).", ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Reset the table SafeSearchService_FtpFiles, this can arise when ftp files are processed but an issue arises.
        /// </summary>
        /// <returns></returns>
        public async Task<bool> ResetFtpDetails()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand comm = new SqlCommand(Properties.Settings.Default.ResetFtpDetails, conn))
                    {
                        comm.CommandType = CommandType.StoredProcedure;
                        await comm.ExecuteNonQueryAsync();
                    }
                }
                return true;
            }
            catch (SqlException ex)
            {
                _logger.Error("Error reseting the table SafeSearchService_FtpFiles. Message ({}).", ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Insert the email\client details into the table SafeSearchService_EmailDetails.
        /// </summary>
        /// <param name="emailId"></param>
        /// <param name="fromAddress"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<bool> InsertEmailDetails(string emailId, string fromAddress, string clientId)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand comm = new SqlCommand(Properties.Settings.Default.InsertEmailDetails, conn))
                    {
                        comm.CommandType = CommandType.StoredProcedure;
                        comm.Parameters.Add("@EmailFrom", SqlDbType.VarChar, 150).Value = fromAddress;
                        comm.Parameters.Add("@EmailId", SqlDbType.VarChar, 250).Value = emailId;
                        comm.Parameters.Add("@ClientId", SqlDbType.VarChar, 10).Value = clientId;
                        await comm.ExecuteNonQueryAsync();
                    }
                }
                return true;
            }
            catch (SqlException ex)
            {
                _logger.Error("Error inserting the email ({}) details. Message ({}). ClientId={ClientId}", fromAddress, ex.Message, clientId);
                return false;
            }
        }

        /// <summary>
        /// Update the table SafeSearchService_EmailDetails to reflect successful processing of the request.
        /// </summary>
        /// <param name="emailId"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<bool> UpdateEmailDetails(string emailId, bool processedStatus)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand comm = new SqlCommand(Properties.Settings.Default.UpdateEmailDetails, conn))
                    {
                        comm.CommandType = CommandType.StoredProcedure;
                        comm.Parameters.Add("@EmailId", SqlDbType.VarChar, 250).Value = emailId;
                        comm.Parameters.Add("@ProcessedStatus", SqlDbType.Bit).Value = processedStatus;
                        await comm.ExecuteNonQueryAsync();
                    }
                }
                return true;
            }
            catch (SqlException ex)
            {
                _logger.Error("Error updating the email details. Message ({})", ex.Message);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<Dictionary<string, string>> FetchClientIdByEmail(string email, string domainName)
        {
            Dictionary<string, string> detailsList = new Dictionary<string, string>();
            try
            {
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand comm = new SqlCommand(Properties.Settings.Default.FetchClientIdByEmail, conn))
                    {
                        comm.CommandType = CommandType.StoredProcedure;
                        comm.Parameters.Add("@Email", SqlDbType.VarChar, 150).Value = email;
                        comm.Parameters.Add("@DomainName", SqlDbType.VarChar, 150).Value = domainName;
                        comm.Parameters.Add("@ClientId", SqlDbType.VarChar, 10).Direction = ParameterDirection.Output;
                        comm.Parameters.Add("@Password", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
                        await comm.ExecuteNonQueryAsync();
                        detailsList.Add("ClientID", (string)comm.Parameters["@ClientId"].Value);
                        detailsList.Add("ClientPwd", (string)comm.Parameters["@Password"].Value);

                        return detailsList;
                    }
                }
            }
            catch (SqlException ex)
            {
                _logger.Error("Error fetching clientId by email for ({}). Message ({})", email, ex.Message);
                return detailsList;
            }
        }

        /// <summary>
        /// Fetch the file name and path of the PDF created by the CreditSafeApi.
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<string> FetchFtpFile(string clientId)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand comm = new SqlCommand(Properties.Settings.Default.FetchFtpFileStoredProcedure, conn))
                    {
                        comm.CommandType = CommandType.StoredProcedure;
                        comm.Parameters.Add("@ClientId", SqlDbType.Int).Value = clientId;
                        var ftpFile = await comm.ExecuteScalarAsync();
                        return (string)ftpFile;
                    }
                }
            }
            catch (SqlException ex)
            {
                _logger.Error("Error fetching ftp file. Message ({}). ClientId={ClientId}", ex.Message, clientId);
                return string.Empty;
            }
        }

        /// <summary>
        /// Mark the record in the table SafeSearchService_FtpFiles as processed.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="ftpFilePath"></param>
        public async void UpdateFtpFiles(string clientId, string ftpFilePath)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand comm = new SqlCommand(Properties.Settings.Default.UpdateFtpFileStoredProcedure, conn))
                    {
                        comm.CommandType = CommandType.StoredProcedure;
                        comm.Parameters.Add("@ClientId", SqlDbType.Int).Value = clientId;
                        comm.Parameters.Add("@FtpFile", SqlDbType.VarChar, 150).Value = ftpFilePath;
                        await comm.ExecuteNonQueryAsync();
                    }
                }

            }
            catch (SqlException ex)
            {
                _logger.Error("Error updating ftp file. Message ({}). ClientId={ClientId}", ex.Message, clientId);

            }
        }

        /// <summary>
        /// Fetch the file paths.
        /// </summary>
        /// <returns></returns>
        public async Task<List<FilePaths>> FetchFilePaths()
        {
            List<FilePaths> filePaths = new List<FilePaths>();
            Extensions extensions = new Extensions();
            try
            {
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand comm = new SqlCommand(Properties.Settings.Default.FetchFilePathsStoredProcedure, conn))
                    {
                        SqlDataReader reader = await comm.ExecuteReaderAsync();
                        if (reader.HasRows)
                        {
                            filePaths = await extensions.DataReaderMapToList<FilePaths>(reader);
                        }
                        return filePaths;
                    }
                }
            }
            catch (SqlException ex)
            {
                _logger.Error("Error fetching list of valid file paths. Message ({}).", ex.Message);
                return null;
            }
        }

        /// <summary>
        /// Retrieve the client details by clientId.
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<ClientDetails> FetchClientDetails(string clientId)
        {
            ClientDetails clientDetails = new ClientDetails();
            try
            {
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand comm = new SqlCommand(Properties.Settings.Default.FetchClientDetails, conn))
                    {
                        comm.CommandType = CommandType.StoredProcedure;
                        comm.Parameters.Add("@ClientId", SqlDbType.Int).Value = clientId;
                        SqlDataReader reader = await comm.ExecuteReaderAsync();
                        if (reader.HasRows)
                        {
                            reader.Read();
                            clientDetails.ClientId = clientId.ToString();
                            clientDetails.Name = reader["Name"].ToString();
                            clientDetails.CompanyName = reader["Company Name"].ToString();
                            clientDetails.Website = reader["Website"].ToString();
                            clientDetails.Email = reader["Email"].ToString();
                            clientDetails.Password = reader["Password"].ToString();
                            clientDetails.ApiKey = reader["ApiKey"].ToString();
                            clientDetails.Active = (bool)reader["Active"];
                        }
                        return clientDetails;
                    }
                }
            }
            catch (SqlException ex)
            {
                _logger.Error(ex, "Error fetching client details. ClientId={ClientId}", clientId);
                return null;
            }
        }

        /// <summary>
        /// Insert the name of the processed file to the table SafeSearchService_EmailDetails
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="emailId"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<bool> InsertProcessedFileName(string fileName, string emailId, string clientId, string folderName)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    conn.Open();
                    using (SqlCommand comm = new SqlCommand(Properties.Settings.Default.InsertFileName, conn))
                    {
                        comm.CommandType = CommandType.StoredProcedure;
                        comm.Parameters.Add("@FileName", SqlDbType.VarChar, 100).Value = fileName;
                        comm.Parameters.Add("@EmailId", SqlDbType.VarChar, 250).Value = emailId;
                        comm.Parameters.Add("@FolderName", SqlDbType.VarChar, 10).Value = folderName;
                        await comm.ExecuteNonQueryAsync();
                    }
                }
                return true;
            }
            catch (SqlException ex)
            {
                _logger.Error("Error inserting the file name ({}) to the SafeSearchService_EmailDetails table. Message ({}). ClientId={ClientId}", fileName, ex.Message, clientId);
                return false;
            }
        }
    }
}