﻿using Microsoft.Exchange.WebServices.Data;
using Microsoft.Identity.Client;
using SafeSearchService.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Logger = NLog.Logger;
using System.IO;
using System.Text.RegularExpressions;
//using Item = Microsoft.Exchange.WebServices.Data.Item;
using System.Linq;
using System.Net.Mail;
using Ionic.Zip;

namespace SafeSearchService.Classes
{
    public class Email
    {
        private string _exchangeEmailPath { get; set; }
        private string _exchangeAddress { get; set; }
        private string _appId { get; set; }
        private string _clientSecret { get; set; }
        private string _tenantId { get; set; }

        readonly SQL _sql;
        private static Logger _logger;

        public Email(string exchangeEmailPath, string exchangeAddress, string tenantId, string appId, string clientSecret, Logger logger)
        {
            _exchangeEmailPath = exchangeEmailPath;
            _exchangeAddress = exchangeAddress;
            _appId = appId;
            _clientSecret = clientSecret;
            _tenantId = tenantId;
            _sql = new SQL(logger);
            _logger = logger;
        }

        /// <summary>
        /// Instantiate a office instance.
        /// </summary>
        /// <returns></returns>
        private async Task<ExchangeService> GetService()
        {
            var identityToken = ConfidentialClientApplicationBuilder
                    .Create(_appId)
                    .WithClientSecret(_clientSecret)
                    .WithTenantId(_tenantId)
                    .Build();

            var scopes = new string[] { "https://outlook.office365.com/.default" };
            var token = await identityToken.AcquireTokenForClient(scopes).ExecuteAsync();

            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2016)
            {
                TraceEnabled = true,
                UseDefaultCredentials = false,
                Credentials = new OAuthCredentials(token.AccessToken),
                Url = new Uri(_exchangeEmailPath)
            };

            return service;
        }

        /// <summary>
        /// Send email detailing why a failed request occured.
        /// </summary>
        /// <param name="messsage"></param>
        /// <param name="clientDetails"></param>
        /// <returns></returns>
        public async Task<bool> SendInvalidAddressEmailToClient(string subject, string header, string emailAddress)
        {
            try
            {
                var htmlBody = "<!DOCTYPE html><html>";
                htmlBody += "<style>div.Head {font-size: 16px; font-family: Tahoma; color: #666666;}";
                htmlBody += "div.Body {font-size: 15px; font-family: Tahoma; color: #666666;}</style>";
                htmlBody += $"<div class=\"Head\"><p>{header}</p></div>";
                
                ExchangeService service = await GetService();
                service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, _exchangeAddress);
                service.HttpHeaders.Add("X-AnchorMailbox", _exchangeAddress);
                var email = new EmailMessage(service)
                {
                    Subject = subject
                };

                email.Body = new MessageBody(BodyType.HTML, htmlBody);
                email.ToRecipients.Add(emailAddress);
                await email.SendAndSaveCopy();

                _logger.Info($"Email successfully sent to ({emailAddress}) detailing why the request failed to send.");

                return true;
            }
            catch (Exception ex)
            {
                _logger.Error($"Error sending the email to ({emailAddress}) detailing why the request failed to send. Message ({ex.Message})");
                return false;
            }
        }

        /// <summary>
        /// Send email detailing why a failed request occured.
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="header"></param>
        /// <param name="messsage"></param>
        /// <param name="clientDetails"></param>
        /// <param name="validationError"></param>
        /// <param name="itemsToProcess"></param>
        /// <returns></returns>
        public async Task<bool>SendEmailToClient(string subject, string header, string messsage, ClientDetails clientDetails, 
            bool validationError, Dictionary<string, string> itemsToProcess)
        {
            try
            {
                var htmlBody = "<!DOCTYPE html><html>";
                htmlBody += "<style>div.Head {font-size: 16px; font-family: Tahoma; color: #666666;}";
                htmlBody += "div.Body {font-size: 15px; font-family: Tahoma; color: #666666;}";
                htmlBody += "table.datatable{border: 1px solid; border-color: #666666;font-size: 14px;font-family: Tahoma; border-collapse: collapse;},";
                htmlBody += "td.datadef{border: 1px solid; padding-left:10pt; font-family:Calibri; font-size: 16px; padding-right:10pt; color:#666666; height:25px;},";
                htmlBody += "th.datahd{border: 1px solid; vertical-align:center; font-family:Calibri; font-size: 18px; color: #666666;}";
                htmlBody += "</style>";
                htmlBody += $"<div class=\"Head\"><p>{header}</p></div>";
               
                if (validationError)
                {
                    htmlBody += "<div class=\"Body\"><ol>";
                    List<string> messages = new List<string>
                        (messsage.Split(new string[] { Environment.NewLine }, StringSplitOptions.None));
                    foreach (string message in messages)
                    {
                        if (!string.IsNullOrEmpty(message))
                        {
                            htmlBody += $"<p><li>{message}</li></p>";
                        }
                    }
                    htmlBody += "</ol></div>";
                    htmlBody += "<div class=\"Body\"><p>The search request parameters are shown below for your reference.</p></div>";
                    htmlBody += "<div class=\"Body\"><table class=\"datatable\"><th class=\"datahd\">Request Parameter</th><th class=\"datahd\">Request Value</th>";
                    foreach (KeyValuePair<string,string> item in itemsToProcess.Where(i=> i.Key !="SheetName"))
                    {
                        if (item.Key == "SettlementAmount")
                        {
                            htmlBody += $"<tr><td class=\"datadef\">{item.Key}</td><td class=\"datadef\">{Convert.ToDouble(item.Value)}</td></tr>";
                        }
                        else
                        {
                            htmlBody += $"<tr><td class=\"datadef\">{item.Key}</td><td class=\"datadef\">{item.Value}</td></tr>";
                        }
                    }
                   
                    htmlBody += "</html>";
                }
                else
                {
                    htmlBody += $"<div class=\"Body\"><p>{Regex.Replace(messsage,"[{}\n\r]","")}</p></div></html>";
                }

                ExchangeService service = await GetService();
                service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, _exchangeAddress);
                service.HttpHeaders.Add("X-AnchorMailbox", _exchangeAddress);
                var email = new EmailMessage(service)
                {
                    Subject = subject
                };

                //Send the email to the client email address if no email included in the template, else send it to the one in the template.
                if (string.IsNullOrEmpty(itemsToProcess.FirstOrDefault(pair => pair.Key == "Email").Value))
                {
                    string[] emailAddresses = clientDetails.Email.Split(';');
                    foreach (string emailaddress in emailAddresses)
                    {
                        email.ToRecipients.Add(emailaddress);
                    }
                }
                else
                {
                    string[] emailAddresses = itemsToProcess.FirstOrDefault(pair => pair.Key == "Email").Value.Split(';');
                    foreach (string emailaddress in emailAddresses)
                    {
                        email.ToRecipients.Add(emailaddress);
                    }
                }
                email.Body = new MessageBody(BodyType.HTML, htmlBody);
                await email.SendAndSaveCopy();

                _logger.Info("{} email successfully sent to {} ClientId={ClientId}", (validationError == true ? "Validation errors" : "Processing errors"), clientDetails.Email, clientDetails.ClientId);

                return true;
            }
            catch (Exception ex)
            {
                _logger.Error("Error sending the email to ({}) detailing why the request failed to send. Message ({})  ClientId={ClientId}", clientDetails.Email, ex.Message, clientDetails.ClientId);
                return false;
            }
        }

        /// <summary>
        /// Search for any emails containing search requests.
        /// </summary>
        /// <returns></returns>
        public async Task<List<ExtractDetails>> SearchForRequests()
        {
            List<ExtractDetails> extractDetailsList = new List<ExtractDetails>();
            List<Item> items = new List<Item>();

            try
            {
                ExchangeService service = await GetService();

                service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, _exchangeAddress);
                service.HttpHeaders.Add("X-AnchorMailbox", _exchangeAddress);

                PropertySet propertySet = new PropertySet(BasePropertySet.FirstClassProperties);
                FolderView folderView = new FolderView(int.MaxValue);
                const Int32 pageSize = 50;
                ItemView itemView = new ItemView(pageSize)
                {
                    PropertySet = propertySet
                };
                FindItemsResults<Item> searchResults = null;
                searchResults = await service.FindItems(WellKnownFolderName.Inbox, itemView);
                foreach (Item item in searchResults.Items)
                {
                    if (item.GetType() == typeof(EmailMessage))
                    {
                        var filePaths = await _sql.FetchFilePaths();

                        if (item.HasAttachments)
                        {
                            var emailId = item.Id;
                            var emailFromAddress = ((EmailMessage)(item)).From.Address;
                            MailAddress address = new MailAddress(emailFromAddress);
                            var clientIdDic = await _sql.FetchClientIdByEmail(emailFromAddress, address.Host);

                            var clientId = clientIdDic.FirstOrDefault(x => x.Key == "ClientID").Value;
                            var clientPwd = clientIdDic.FirstOrDefault(x => x.Key == "ClientPwd").Value;

                            var extractedFileLocation = await ExtractAttachment(item, $"{filePaths.Find(f => f.PathName == "SafeSearch").PathFilePath}{Properties.Settings.Default.ExtractFolder}\\{clientId}",
                                $"{filePaths.Find(f => f.PathName == "SafeSearch").PathFilePath}{Properties.Settings.Default.TempFolder}\\{clientId}",clientId,
                                clientPwd);

                            if (!string.IsNullOrEmpty(clientId) && emailFromAddress != "reports@dashrosolutions.com")
                            {
                                if (!await _sql.IsEmailProcessed(emailId.ToString()))
                                {
                                    bool inserted = !string.IsNullOrEmpty(extractedFileLocation) && await _sql.InsertEmailDetails(emailId.ToString(), emailFromAddress, clientId);
                                    if (inserted)
                                    {
                                        if (!string.IsNullOrEmpty(extractedFileLocation))
                                        {
                                            ExtractDetails extractDetails = new ExtractDetails()
                                            {
                                                ClientId = clientId,
                                                ExtractPath = extractedFileLocation,
                                                EmailId = item.Id.ToString()
                                            };
                                            extractDetailsList.Add(extractDetails);
                                        };

                                        _logger.Info("Located email with the Id ({}), sent from ({}), request type ({}). ClientId={ClientId}", emailId, emailFromAddress, item.Subject, clientId);
                                    }
                                }
                                else
                                {
                                    _logger.Info("Email with the Id ({}) has previously been processed. ClientId={ClientId}", emailId, clientId);
                                }
                            }
                            else //Client not recognised!
                            {
                                await _sql.UpdateEmailDetails(emailId.ToString(), false);
                                MoveProcessedFiles(extractedFileLocation, Properties.Settings.Default.ErrorFolder, string.Empty, emailId.ToString());
                                await MoveProcessedEmail(emailId.ToString(), true, string.Empty);
                                _logger.Info("Unable to process the file ({}) as the sent from email address not recognised ({}).", extractedFileLocation, emailFromAddress);
                                await SendInvalidAddressEmailToClient($"Unable to process the request received from {emailFromAddress}",$"The request received from {emailFromAddress} was from an email address that is not recognised.  Requests can only be from an email address that has been previously authenticated, if you have any issues please contact Dashro Solutions for further advice.", emailFromAddress);

                            }
                        }
                    }
                }

                return extractDetailsList;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, "Error searching the folder for any items.");
                return null;
            }
        }

        /// <summary>
        /// Extract the attachment from the email.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="filepath"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        private async Task<string> ExtractAttachment(Item item, string filepath, string tempFilePath, string clientId, string clientPassword)
        {
            var filename = $"{clientId}_{(string.IsNullOrEmpty(item.Subject) ? "No_Subject_Found" : item.Subject.Replace(" ", "").Replace(",", "").Trim())}_{String.Format("{0:ddMMyyyy_HHmmss}", DateTime.Now)}.xlsm";
            var filenameCorrected = string.Join("_", filename.Split(Path.GetInvalidFileNameChars()));
            var fileSaveLocation = string.Concat(filepath, "\\", filenameCorrected);
            var extractedFileLocation = string.Empty;

            try
            {
                if (!Directory.Exists(tempFilePath))
                {
                    Directory.CreateDirectory(tempFilePath);
                }

                if (!Directory.Exists(filepath))
                {
                    Directory.CreateDirectory(filepath);
                }

                if(item.HasAttachments)
                {
                   await item.Load();

                   foreach (Microsoft.Exchange.WebServices.Data.Attachment attachment in item.Attachments)
                   {
                        FileAttachment fileAttachment = attachment as FileAttachment;

                        if (attachment.ContentType == "application/x-zip-compressed")
                        {
                            await fileAttachment.Load();
                            using (FileStream fileStream = new FileStream($"{fileSaveLocation.Replace(".xlsm",".zip")}", FileMode.Create))
                            {
                                using (BinaryWriter writer = new BinaryWriter(fileStream))
                                {
                                    writer.Write(fileAttachment.Content);
                                }
                            }

                            //Extract the zip file.
                            using (ZipFile finZip = ZipFile.Read(fileSaveLocation.Replace(".xlsm", ".zip")))
                            {
                                finZip.Password = clientPassword;
                                finZip.ExtractAll(fileSaveLocation.Replace(".xlsm", ""), ExtractExistingFileAction.OverwriteSilently);
                            }

                            //Now delete and move the file/folder the .zip file.
                            File.Delete(fileSaveLocation.Replace(".xlsm", ".zip"));

                            foreach (string file in Directory.GetFiles(fileSaveLocation.Replace(".xlsm", "")))
                            {
                                extractedFileLocation = $"{filepath}\\{clientId}_{(string.IsNullOrEmpty(item.Subject) ? "No_Subject_Found" : item.Subject.Replace(" ", "").Trim())}_{String.Format("{0:ddMMyyyy_HHmmss}", DateTime.Now)}.xlsm";
                                System.Threading.Thread.Sleep(1000);
                                File.Copy(Path.GetFullPath(file), extractedFileLocation);
                                //System.Threading.Thread.Sleep(1000);
                            }

                            DirectoryInfo directory = new DirectoryInfo(filepath);
                            foreach (DirectoryInfo dir in directory.EnumerateDirectories())
                            {
                                dir.Delete(true);
                            }
                            return extractedFileLocation;
                        }
                        else
                        {
                            await fileAttachment.Load();
                            using (FileStream fileStream = new FileStream($"{fileSaveLocation}", FileMode.Create))
                            {
                                using (BinaryWriter writer = new BinaryWriter(fileStream))
                                {
                                    writer.Write(fileAttachment.Content);
                                    _logger.Info("Extracted the attachment from the email id ({}) with subject ({}) to ({}). ClientId={ClientId}", item.Id, item.Subject, fileSaveLocation, clientId);
                                    writer.Close();
                                    System.Threading.Thread.Sleep(2000);

                                    //Unprotect the File.
                                    System.Diagnostics.Process process = new System.Diagnostics.Process();
                                    process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                    process.StartInfo.FileName = "C:\\Gordons Work\\UnprotectFile\\UnprotectFile\\bin\\Debug\\netcoreapp3.1\\UnprotectFile.exe";
                                    process.StartInfo.Arguments = $"{fileSaveLocation} {clientPassword}";
                                    process.Start();
                                    var inProcess = true;
                                    while (inProcess)
                                    {
                                        process.Refresh();
                                        System.Threading.Thread.Sleep(10);
                                        if (process.HasExited)
                                        {
                                            inProcess = false;
                                        }
                                    }

                                    return fileSaveLocation;
                                }
                            }
                        }
                   }
                }

                return String.Empty;
            }
            catch (Exception ex)
            {
                _logger.Error( "Error extracting the attachment from the email id ({}) with subject ({}) | Error Message {ex.Message}. ClientId={ClientId}", item.Id, item.Subject, ex.Message, clientId);
                return String.Empty;
            }
        }

        /// <summary>
        ///  Save the attachment to the Temp Folder to check if this is a Search Request or Swift File.
        /// </summary>
        /// <param name="fileAttachment"></param>
        /// <param name="tempFilePath"></param>
        /// <param name="subject"></param>
        /// <returns></returns>
        //private async Task<bool> SaveToTempFolder(FileAttachment fileAttachment, string tempFilePath, string subject, string clientId)
        //{
        //    var filename = $"Temp_{subject}.xlsm";
        //    var filenameCorrected = string.Join("_", filename.Split(Path.GetInvalidFileNameChars()));
        //    var fileSaveLocation = string.Concat(tempFilePath, "\\", filenameCorrected);

        //    try
        //    {
        //        await fileAttachment.Load();
        //        using (FileStream fileStream = new FileStream($"{fileSaveLocation}", FileMode.Create))
        //        {
        //            using (BinaryWriter writer = new BinaryWriter(fileStream))
        //            {
        //                writer.Write(fileAttachment.Content);
        //                writer.Close();
        //            }

        //            GenericFunctions genericFunctions = new GenericFunctions(_logger);
        //            var results = genericFunctions.GetAllWorksheets(fileSaveLocation);

        //            //Delete files from the Temp folder
        //            File.Delete(fileSaveLocation);
        //            return results;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Error("Error ({}) saving the file ({}) to the Temp Folder. ClientId={ClientId}", ex.Message, filename, clientId);
        //        return false;
        //    }
        //}

        /// <summary>
        /// Move the emails from the Inbox to the Processed Requests once finished processing.
        /// </summary>
        /// <param name="emailId"></param>
        /// <param name="processFailed"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public async Task<bool> MoveProcessedEmail(string emailId, bool processFailed, string clientId ="")
        {
            try
            {
                ExchangeService service = await GetService();

                service.ImpersonatedUserId = new ImpersonatedUserId(ConnectingIdType.SmtpAddress, _exchangeAddress);
                service.HttpHeaders.Add("X-AnchorMailbox", _exchangeAddress);

                FolderView folderView = new FolderView(int.MaxValue)
                {
                    Traversal = FolderTraversal.Deep
                };

                //Seach filter for the folder.
                SearchFilter searchFilterProcessed = new SearchFilter.IsEqualTo(FolderSchema.DisplayName, "Processed Requests");
                SearchFilter searchFilterFailed = new SearchFilter.IsEqualTo(FolderSchema.DisplayName, "Failed Requests");

                //Locate the folder.
                FindFoldersResults findFolderResultsProcessed = await service.FindFolders(WellKnownFolderName.Inbox, searchFilterProcessed, folderView);
                FindFoldersResults findFolderResultsFailed = await service.FindFolders(WellKnownFolderName.Inbox, searchFilterFailed, folderView);

                PropertySet propertySet = new PropertySet(BasePropertySet.FirstClassProperties);
                const Int32 pageSize = 50;
                ItemView itemView = new ItemView(pageSize)
                {
                    PropertySet = propertySet
                };
                FindItemsResults<Item> searchResults = null;
                Folder folderProcessed = findFolderResultsProcessed.Folders[0];
                Folder folderFailed = findFolderResultsFailed.Folders[0];
                searchResults = await service.FindItems(WellKnownFolderName.Inbox, itemView);
                foreach (Item item in searchResults.Items)
                {
                    if (item.Id.ToString() == emailId)
                    {
                        if (processFailed)
                        {
                            await item.Move(folderFailed.Id);
                        }
                        else
                        {
                            await item.Move(folderProcessed.Id);
                        }

                        if (!string.IsNullOrEmpty(clientId))
                        {
                            _logger.Info("Moved the email with the subject ({}) sent by ({}) from the Inbox folder to Processed Requests. ClientId={ClientId}", item.Subject, ((EmailMessage)(item)).From.Address, clientId);
                        }
                        else
                        {
                            _logger.Info("Moved the email with the subject ({}) sent by ({}) from the Inbox folder to Failed Requests, as the email address is not recognised.", item.Subject, ((EmailMessage)(item)).From.Address);
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.Info("Error moving the email with the Id ({}) from the Search Requests folder to Processed Requests. Message ({}). ClientId={ClientId}", emailId, ex.Message, clientId);
                return false;
            }
        }

        /// <summary>
        /// Move the extract files once processed.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="folderName"></param>
        public async void MoveProcessedFiles(string filePath, string folderName, string clientId, string emailId)
        {
            try
            {
                var moveToFolder = Path.GetDirectoryName(filePath).Replace("Extracts", folderName);
                if (!Directory.Exists(moveToFolder))
                {
                    Directory.CreateDirectory(moveToFolder);
                }

                var filename = Path.GetFileName(filePath);

                if (!string.IsNullOrEmpty(clientId))
                {
                    //Insert the name of the processed file to the table SafeSearchService_EmailDetails
                    await _sql.InsertProcessedFileName(filename, emailId, clientId, folderName);
                }

                //Move the processed file to either the Processed or Error folder.
                File.Move(filePath, string.Concat(moveToFolder, "\\", filename));

                _logger.Info("The file ({}) was moved from ({}) to ({}).  ClientId={ClientId}", Path.GetFileName(filePath), Path.GetDirectoryName(filePath), moveToFolder, clientId);
            }
            catch(IOException ex)
            {
                _logger.Error("Error occured moving the file ({}), Error message ({}). ClientId={ClientId} ", Path.GetFileName(filePath), ex.Message, clientId);
            }
        }
    }
}