﻿using NLog;
using SafeSearchService.Classes;
using SafeSearchService.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using static SafeSearchService.Classes.Errors;

namespace SafeSearchService
{
    public partial class SafeSearchService : ServiceBase
    {
        private string _exchangeEmailPath { get; set; }
        private string _exchangeAddress { get; set; }
        private string _tenantId { get; set; }
        private string _appId { get; set; }
        private string _clientSecret { get; set; }
        private int _ProcessCount { get; set; }
        private int _ProcessFtpClientCount { get; set; }
        private int _ProcessFtpFileCount { get; set; }
        private int _ProcessFtpSheetCount { get; set; }
        private int _ProcessItemsCount { get; set; }
        private bool _movedToProcessedFolder { get; set; }

        Timer _tmrExecuteProcess = new Timer();
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger(); //On rebuild or pull from master, ensure nlog.config is copied to bin/debug or bin/release folder!

        public SafeSearchService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
#if (DEBUG)
            Debugger.Launch();
#endif
            _logger.Info("SafeSearchService Starting");
            _exchangeEmailPath = Properties.Settings.Default.ExchangeEmailPath;
            _exchangeAddress = Properties.Settings.Default.ExchangeAddress;
            _tenantId = Properties.Settings.Default.TenantId;
            _appId = Properties.Settings.Default.AppId;
            _clientSecret = Properties.Settings.Default.ClientSecret;

            _tmrExecuteProcess = new Timer
            {
                Interval = Convert.ToDouble(Properties.Settings.Default.TimerInterval),
                Enabled = true
            };
            _tmrExecuteProcess.Start();
            _tmrExecuteProcess.Elapsed += new ElapsedEventHandler(this._tmrExecuteProcess_Tick);
        }

        private async void _tmrExecuteProcess_Tick(object sender, EventArgs e)
        {
            try
            {
                _logger.Info("Commence checking for emails");
                _ProcessCount = 0;

                //_tmrExecuteProcess.Enabled = false;

                //Create a new instance of the Email Class.
                Email email = new Email(_exchangeEmailPath, _exchangeAddress, _tenantId, _appId, _clientSecret, _logger);

                //Create a new instance of the SQL Class.
                SQL sql = new SQL(_logger);

                //Fetch the File Paths.
                var filePaths = await sql.FetchFilePaths();

                //Create a new instance of the Excel Class.
                Excel excel = new Excel(_logger);

                //Create a new instance of the Excel Class.
                JSON json = new JSON();

                //Create a new instance of the SafeSearch Class.
                SafeSearch safeSearch = new SafeSearch(_logger);

                //Create a new Instance of the Generic Functions Class.
                GenericFunctions genericFunctions = new GenericFunctions(_logger);

                //Reset any failed Emails.
                bool resetEmails = await sql.ResetEmailDetails();
                if(!resetEmails)
                {
                    _logger.Error($"Error whilst executing the stored procedure {Properties.Settings.Default.ResetEmailDetails}.");
                }

                //Reset any failed Ftp.
                bool resetFtp = await sql.ResetFtpDetails();
                if (!resetFtp)
                {
                    _logger.Error($"Error whilst executing the stored procedure {Properties.Settings.Default.ResetFtpDetails}.");
                }

                //Request request = new Request();
                List<ExtractDetails> extractDetailsList = await email.SearchForRequests();

                if (extractDetailsList.Count > 0)
                {
                    //Process each of the extracted files from the email.
                    foreach (ExtractDetails extractDetails in extractDetailsList)
                    {
                        _ProcessCount += 1;

                        //Fetch the Client Details.
                        ClientDetails clientDetails = await sql.FetchClientDetails(extractDetails.ClientId);

                        if (clientDetails.Active)  //Only process when the client is active.
                        {
                            var currentFileForProcessing = extractDetails.ExtractPath;

                            //Populate the request model with the required parameters.
                            List<Dictionary<string, string>> requestParameters = new List<Dictionary<string, string>>();
                            requestParameters = await excel.ExtractRequestValues(currentFileForProcessing, clientDetails.ClientId);

                            //Itterate through the items to process.
                            foreach (Dictionary<string, string> itemToProcess in requestParameters)
                            {
                                _ProcessItemsCount += 1;

                                if (itemToProcess.ContainsKey("Search Type")) //We are processing Search Types.
                                {
                                    //Set the Search Type value.
                                    extractDetails.SearchType = itemToProcess.FirstOrDefault(pair => pair.Key == "Search Type").Value;

                                    //Perform validation on the submitted request and handle any errors by emailing the client of any issues.
                                    var validationErrors = ValidateRequest(itemToProcess, extractDetails.SearchType, false);

                                    if (validationErrors.Length == 0)
                                    {
                                        SearchRequest searchRequest = new SearchRequest
                                        {
                                            Threshold = Convert.ToInt32(itemToProcess.FirstOrDefault(pair => pair.Key == "Threshold").Value),
                                            Pep = ReturnBoolValue(Convert.ToInt32(itemToProcess.FirstOrDefault(pair => pair.Key == "Pep").Value)),
                                            PreviousSanctions = ReturnBoolValue(Convert.ToInt32(itemToProcess.FirstOrDefault(pair => pair.Key == "Previous Sanctions").Value)),
                                            CurrentSanctions = ReturnBoolValue(Convert.ToInt32(itemToProcess.FirstOrDefault(pair => pair.Key == "Current Sanctions").Value)),
                                            LawEnforcement = ReturnBoolValue(Convert.ToInt32(itemToProcess.FirstOrDefault(pair => pair.Key == "Law Enforcement").Value)),
                                            FinancialRegulator = ReturnBoolValue(Convert.ToInt32(itemToProcess.FirstOrDefault(pair => pair.Key == "Financial Regulator").Value)),
                                            Insolvency = ReturnBoolValue(Convert.ToInt32(itemToProcess.FirstOrDefault(pair => pair.Key == "Insolvency").Value)),
                                            DisqualifiedDirector = ReturnBoolValue(Convert.ToInt32(itemToProcess.FirstOrDefault(pair => pair.Key == "Disqualified Director").Value)),
                                            AdverseMedia = ReturnBoolValue(Convert.ToInt32(itemToProcess.FirstOrDefault(pair => pair.Key == "Adverse Media").Value)),
                                            Forename = itemToProcess.FirstOrDefault(pair => pair.Key == "Forename").Value,
                                            Middlename = itemToProcess.FirstOrDefault(pair => pair.Key == "Middlename").Value,
                                            Surname = itemToProcess.FirstOrDefault(pair => pair.Key == "Surname").Value,
                                            BusinessName = itemToProcess.FirstOrDefault(pair => pair.Key == "Business Name").Value,
                                            DateOfBirth = itemToProcess.FirstOrDefault(pair => pair.Key == "Date of Birth").Value,
                                            YearOfBirth = itemToProcess.FirstOrDefault(pair => pair.Key == "Year of Birth").Value,
                                            DateOfDeath = itemToProcess.FirstOrDefault(pair => pair.Key == "Date of Death").Value,
                                            YearOfDeath = itemToProcess.FirstOrDefault(pair => pair.Key == "Year of Death").Value,
                                            Address = itemToProcess.FirstOrDefault(pair => pair.Key == "Address").Value,
                                            City = itemToProcess.FirstOrDefault(pair => pair.Key == "City").Value,
                                            County = itemToProcess.FirstOrDefault(pair => pair.Key == "County").Value,
                                            PostCode = itemToProcess.FirstOrDefault(pair => pair.Key == "Post Code").Value,
                                            Country = itemToProcess.FirstOrDefault(pair => pair.Key == "Country").Value,
                                            Email = itemToProcess.FirstOrDefault(pair => pair.Key == "Email").Value,
                                            Ftp = false
                                        };

                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(searchRequest), clientDetails.ClientId);

                                        //Send the Payload to the endpoint.
                                        Response responseBody = await safeSearch.SubmitRequest(searchRequest, clientDetails, extractDetails.SearchType);

                                        //For a successful request the record can be marked as processed.
                                        if (responseBody.ResponseCode == 200)
                                        {
                                            await sql.UpdateEmailDetails(extractDetails.EmailId, true);
                                            email.MoveProcessedFiles(extractDetails.ExtractPath, Properties.Settings.Default.ProcessedFolder, extractDetails.ClientId, extractDetails.EmailId);
                                            await email.MoveProcessedEmail(extractDetails.EmailId, false, extractDetails.ClientId);
                                            _logger.Info("Completed processing of the file ({}). ClientId={ClientId}", extractDetails.ExtractPath, extractDetails.ClientId);
                                        }
                                        else
                                        {
                                            await sql.UpdateEmailDetails(extractDetails.EmailId, false);
                                            email.MoveProcessedFiles(extractDetails.ExtractPath, Properties.Settings.Default.ErrorFolder, extractDetails.ClientId, extractDetails.EmailId);
                                            await email.MoveProcessedEmail(extractDetails.EmailId, true, extractDetails.ClientId);

                                            _logger.Info("Unable to process the file ({}) as a result of the following error ({}). ClientId={ClientId}", extractDetails.ExtractPath, responseBody.Message, extractDetails.ClientId);
                                            if (responseBody.Message.Contains("message"))
                                            {
                                                var subject = $"Unable to proceed with the {extractDetails.SearchType} Search Request requested by {clientDetails.Name}";
                                                var header = $"The following error ocurred when processing the {extractDetails.SearchType} Search Request requested by {clientDetails.Name}. Please contact Dashro Solutions for further details.";
                                                int trimLength = responseBody.Message.Length - 14;
                                                var result = await email.SendEmailToClient(subject, header, responseBody.Message.Substring(12, trimLength), clientDetails, false, itemToProcess);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        await sql.UpdateEmailDetails(extractDetails.EmailId, false);
                                        email.MoveProcessedFiles(extractDetails.ExtractPath, Properties.Settings.Default.ErrorFolder, extractDetails.ClientId, extractDetails.EmailId);
                                        await email.MoveProcessedEmail(extractDetails.EmailId, true, extractDetails.ClientId);
                                        _logger.Info("Unable to process the file ({}) as a result of the following error ({}). ClientId={ClientId}", extractDetails.ExtractPath, validationErrors, extractDetails.ClientId);
                                        var subject = $"Validation errors were found in the {extractDetails.SearchType} Search Request requested by {clientDetails.Name}";
                                        var header = $"The following validation errors were found in the {extractDetails.SearchType} Search Request requested by {clientDetails.Name}.  Please correct these and resubmit your request.";
                                        var result = await email.SendEmailToClient(subject, header, validationErrors, clientDetails, true, itemToProcess);
                                    }
                                }
                                else //Swift Files.
                                {
                                    Swift swift = new Swift(_logger);
                                    Response responseBody = new Response();

                                    //Perform validation on the submitted request and handle any errors by emailing the client of any issues.
                                    var validationErrors = ValidateSwift(itemToProcess, genericFunctions);

                                    if (validationErrors.Length == 0)
                                    {
                                        string swiftType = itemToProcess.FirstOrDefault(pair => pair.Key == "SheetName").Value;
                                        if (swiftType == SwiftTypes.MT101)
                                        {
                                            MT101 mt101 = swift.CreateMT101Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt101), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt101, clientDetails, SwiftTypes.MT101, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT103)
                                        {
                                            MT103 mt103 = swift.CreateMT103Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt103), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt103, clientDetails, SwiftTypes.MT103, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT192)
                                        {
                                            MT192 mt192 = swift.CreateMT192Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt192), clientDetails.ClientId);

                                            //Send the payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt192, clientDetails, SwiftTypes.MT192, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT199)
                                        {
                                            MT199 mt199 = swift.CreateMT199Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt199), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt199, clientDetails, SwiftTypes.MT199, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT200)
                                        {
                                            MT200 mt200 = swift.CreateMT200Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt200), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt200, clientDetails, SwiftTypes.MT200, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT202)
                                        {
                                            MT202 mt202 = swift.CreateMT202Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt202), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt202, clientDetails, SwiftTypes.MT202, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT210)
                                        {
                                            MT210 mt210 = swift.CreateMT210Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt210), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt210, clientDetails, SwiftTypes.MT210, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT299)
                                        {
                                            MT299 mt299 = swift.CreateMT299Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt299), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt299, clientDetails, SwiftTypes.MT299, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT380)
                                        {
                                            MT380 mt380 = swift.CreateMT380Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt380), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt380, clientDetails, SwiftTypes.MT380, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT399)
                                        {
                                            MT399 mt399 = swift.CreateMT399Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt399), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt399, clientDetails, SwiftTypes.MT399, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT499)
                                        {
                                            MT499 mt499 = swift.CreateMT499Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt499), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt499, clientDetails, SwiftTypes.MT499, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT502)
                                        {
                                            MT502 mt502 = swift.CreateMT502Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt502), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt502, clientDetails, SwiftTypes.MT502, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT540)
                                        {
                                            MT540 mt540 = swift.CreateMT540Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt540), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt540, clientDetails, SwiftTypes.MT540, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT541)
                                        {
                                            MT541 mt541 = swift.CreateMT541Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt541), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt541, clientDetails, SwiftTypes.MT541, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT542)
                                        {
                                            MT542 mt542 = swift.CreateMT542Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt542), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt542, clientDetails, SwiftTypes.MT542, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT543)
                                        {
                                            MT543 mt543 = swift.CreateMT543Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt543), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt543, clientDetails, SwiftTypes.MT543, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT545)
                                        {
                                            MT545 mt545 = swift.CreateMT545Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt545), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt545, clientDetails, SwiftTypes.MT545, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT547)
                                        {
                                            MT547 mt547 = swift.CreateMT547Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt547), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt547, clientDetails, SwiftTypes.MT547, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT548)
                                        {
                                            MT548 mt548 = swift.CreateMT548Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created.  ClientId={ClientId}", json.Serializer(mt548), clientDetails.ClientId);

                                            //Send the payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt548, clientDetails, SwiftTypes.MT548, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT565)
                                        {
                                            MT565 mt565 = swift.CreateMT565Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt565), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt565, clientDetails, SwiftTypes.MT565, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT599)
                                        {
                                            MT599 mt599 = swift.CreateMT599Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt599), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt599, clientDetails, SwiftTypes.MT599, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT699)
                                        {
                                            MT699 mt699 = swift.CreateMT699Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt699), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt699, clientDetails, SwiftTypes.MT699, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT799)
                                        {
                                            MT799 mt799 = swift.CreateMT799Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt799), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt799, clientDetails, SwiftTypes.MT799, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT899)
                                        {
                                            MT899 mt899 = swift.CreateMT899Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt899), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt899, clientDetails, SwiftTypes.MT899, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT920)
                                        {
                                            MT920 mt920 = swift.CreateMT920Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt920), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt920, clientDetails, SwiftTypes.MT920, filePaths);
                                        }
                                        else if (swiftType == SwiftTypes.MT999)
                                        {
                                            MT999 mt999 = swift.CreateMT999Payload(itemToProcess, clientDetails.ClientId, false);
                                            _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt999), clientDetails.ClientId);

                                            //Send the Payload to the endpoint.
                                            responseBody = await safeSearch.SubmitSwiftRequest(mt999, clientDetails, SwiftTypes.MT999, filePaths);
                                        }
                                        if (responseBody.ResponseCode == 200)
                                        {
                                            await sql.UpdateEmailDetails(extractDetails.EmailId, true);
                                            email.MoveProcessedFiles(extractDetails.ExtractPath, Properties.Settings.Default.ProcessedFolder, extractDetails.ClientId, extractDetails.EmailId);
                                            await email.MoveProcessedEmail(extractDetails.EmailId, false, extractDetails.ClientId);
                                            _logger.Info("Completed processing of the file ({}). ClientId={ClientId}", extractDetails.ExtractPath, extractDetails.ClientId);
                                        }
                                        else
                                        {
                                            await sql.UpdateEmailDetails(extractDetails.EmailId, false);
                                            email.MoveProcessedFiles(extractDetails.ExtractPath, Properties.Settings.Default.ErrorFolder, extractDetails.ClientId, extractDetails.EmailId);
                                            await email.MoveProcessedEmail(extractDetails.EmailId, true, extractDetails.ClientId);

                                            _logger.Info("Unable to process the file ({}) as a result of the following error ({}). ClientId={ClientId}", extractDetails.ExtractPath, responseBody.Message, extractDetails.ClientId);
                                            if (responseBody.Message.Contains("message"))
                                            {
                                                var subject = $"Unable to proceed with the {extractDetails.SearchType} Swift Request requested by {clientDetails.Name}";
                                                var header = $"The following error ocurred when processing the {swiftType} Swift Request requested by {clientDetails.Name}. Please contact Dashro Solutions for further details.";
                                                int trimLength = responseBody.Message.Length - 14;
                                                var result = await email.SendEmailToClient(subject, header, responseBody.Message.Substring(12, trimLength), clientDetails, false, itemToProcess);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        await sql.UpdateEmailDetails(extractDetails.EmailId, false);
                                        email.MoveProcessedFiles(extractDetails.ExtractPath, Properties.Settings.Default.ErrorFolder, extractDetails.ClientId, extractDetails.EmailId);
                                        await email.MoveProcessedEmail(extractDetails.EmailId, true, extractDetails.ClientId);
                                        _logger.Info("Unable to process the file ({}) as a result of the following error ({}). ClientId={ClientId}", extractDetails.ExtractPath, validationErrors, extractDetails.ClientId);
                                        var subject = $"Validation errors were found in the {extractDetails.SearchType} Swift Request requested by {clientDetails.Name}";
                                        var header = $"The following validation errors were found in the {itemToProcess.FirstOrDefault(pair => pair.Key == "SheetName").Value} Swift Request requested by {clientDetails.Name}.  Please correct these and resubmit your request.";
                                        var result = await email.SendEmailToClient(subject, header, validationErrors, clientDetails, true, itemToProcess);
                                    }
                                }
                            }
                        }
                    }

                    _logger.Info($"Completed checking for emails, ({_ProcessCount}) emails processed.");
                }
                else
                {
                    _logger.Info($"Completed checking, no emails found");
                }


                //  ********  Check for any ftp files *********.
                try
                {
                    _logger.Info("Commence checking for FTP files.");
                    _ProcessFtpClientCount = 0;
                    _ProcessFtpFileCount = 0;

                    Ftp ftp = new Ftp(_logger);

                    //First get a list of all clients who have an FTP folder.
                    List<string> ClientIds = ftp.GetAllFtpClients();

                    if (ClientIds.Count > 0)
                    {

                        //Itterate through each client Id.
                        foreach (string clientId in ClientIds)
                        {
                            _ProcessFtpClientCount += 1;

                            //Fetch the Client Details.
                            ClientDetails clientDetails = await sql.FetchClientDetails(clientId);

                            //Only check for active accounts
                            if (clientDetails.Active)
                            {
                                //Create a list of files from the client.
                                List<string> clientFiles = ftp.ListFtpClientFiles($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpServerFolder}/", clientId);

                                if (clientFiles.Count > 0)
                                {
                                    Dictionary<string, string> FilesToProcess = new Dictionary<string, string>();

                                    foreach (string clientFile in clientFiles)
                                    {
                                        _ProcessFtpFileCount += 1;

                                        //Search for any files and extract these.
                                        string ClientExtractFile = ftp.ExtractFtpClientFiles($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpServerFolder}/{clientFile}",
                                            clientDetails.ClientId, filePaths.Find(f => f.PathName == "SafeSearch").PathFilePath, clientDetails.Password);

                                        _logger.Info("The file ({}) located in ({}). ClientId={ClientId}", clientFile, Properties.Settings.Default.FtpServerFolder, clientId);

                                        if (!string.IsNullOrEmpty(ClientExtractFile))
                                        {
                                            //Add the files to the dictionary object FilesToProcess.
                                            FilesToProcess.Add(clientFile, ClientExtractFile);
                                        }
                                    }

                                    //Populate the request model with the required parameters.
                                    foreach (KeyValuePair<string, string> fileToProcess in FilesToProcess)
                                    {
                                        List<Dictionary<string, string>> requestParameters = new List<Dictionary<string, string>>();
                                        requestParameters = await excel.ExtractRequestValues(fileToProcess.Value, clientDetails.ClientId);

                                        foreach (Dictionary<string, string> itemToProcess in requestParameters)
                                        {
                                            if (itemToProcess.ContainsKey("Search Type")) //We are processing Search Types.
                                            {
                                                _ProcessFtpSheetCount += 1;

                                                string searchType = itemToProcess.FirstOrDefault(pair => pair.Key == "Search Type").Value;

                                                //Perform validation on the submitted request and handle any errors by emailing the client of any issues.
                                                var validationErrors = ValidateRequest(itemToProcess, searchType, true);

                                                if (validationErrors.Length == 0)
                                                {
                                                    SearchRequest request = new SearchRequest
                                                    {
                                                        Threshold = Convert.ToInt32(itemToProcess.FirstOrDefault(pair => pair.Key == "Threshold").Value),
                                                        Pep = ReturnBoolValue(Convert.ToInt32(itemToProcess.FirstOrDefault(pair => pair.Key == "Pep").Value)),
                                                        PreviousSanctions = ReturnBoolValue(Convert.ToInt32(itemToProcess.FirstOrDefault(pair => pair.Key == "Previous Sanctions").Value)),
                                                        CurrentSanctions = ReturnBoolValue(Convert.ToInt32(itemToProcess.FirstOrDefault(pair => pair.Key == "Current Sanctions").Value)),
                                                        LawEnforcement = ReturnBoolValue(Convert.ToInt32(itemToProcess.FirstOrDefault(pair => pair.Key == "Law Enforcement").Value)),
                                                        FinancialRegulator = ReturnBoolValue(Convert.ToInt32(itemToProcess.FirstOrDefault(pair => pair.Key == "Financial Regulator").Value)),
                                                        Insolvency = ReturnBoolValue(Convert.ToInt32(itemToProcess.FirstOrDefault(pair => pair.Key == "Insolvency").Value)),
                                                        DisqualifiedDirector = ReturnBoolValue(Convert.ToInt32(itemToProcess.FirstOrDefault(pair => pair.Key == "Disqualified Director").Value)),
                                                        AdverseMedia = ReturnBoolValue(Convert.ToInt32(itemToProcess.FirstOrDefault(pair => pair.Key == "Adverse Media").Value)),
                                                        Forename = itemToProcess.FirstOrDefault(pair => pair.Key == "Forename").Value,
                                                        Middlename = itemToProcess.FirstOrDefault(pair => pair.Key == "Middlename").Value,
                                                        Surname = itemToProcess.FirstOrDefault(pair => pair.Key == "Surname").Value,
                                                        BusinessName = itemToProcess.FirstOrDefault(pair => pair.Key == "Business Name").Value,
                                                        DateOfBirth = itemToProcess.FirstOrDefault(pair => pair.Key == "Date of Birth").Value,
                                                        YearOfBirth = itemToProcess.FirstOrDefault(pair => pair.Key == "Year of Birth").Value,
                                                        DateOfDeath = itemToProcess.FirstOrDefault(pair => pair.Key == "Date of Death").Value,
                                                        YearOfDeath = itemToProcess.FirstOrDefault(pair => pair.Key == "Year of Death").Value,
                                                        Address = itemToProcess.FirstOrDefault(pair => pair.Key == "Address").Value,
                                                        City = itemToProcess.FirstOrDefault(pair => pair.Key == "City").Value,
                                                        County = itemToProcess.FirstOrDefault(pair => pair.Key == "County").Value,
                                                        PostCode = itemToProcess.FirstOrDefault(pair => pair.Key == "Post Code").Value,
                                                        Country = itemToProcess.FirstOrDefault(pair => pair.Key == "Country").Value,
                                                        Ftp = true
                                                    };

                                                    _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(request), clientId);

                                                    //Send the Payload to the endpoint.
                                                    Response responseBody = await safeSearch.SubmitRequest(request, clientDetails, searchType);

                                                    if (responseBody.ResponseCode == 200)
                                                    {
                                                        //Fetch the file details of the file to be copied to the client folder.
                                                        string pdfFileName = await sql.FetchFtpFile(clientId);

                                                        //Copy the PDF file to the to_client Folder.
                                                        bool result = ftp.CopyFileToFtp($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpClientFolder}/{Path.GetFileName(pdfFileName)}", pdfFileName, clientId);

                                                        if (result)
                                                        {
                                                            _logger.Info("Copied the file ({}) to the clients to_client folder. ClientId={ClientId}", pdfFileName, clientId);

                                                            //Mark the file as Processed.
                                                            sql.UpdateFtpFiles(clientId, pdfFileName);

                                                            //Delete the file from the FTP Folder.
                                                            ftp.DeleteFtpFileFromFolder($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpServerFolder}/{fileToProcess.Key}", clientId);

                                                            //Copy the PDF file to the to_client Folder.
                                                            _movedToProcessedFolder = ftp.CopyFileToFtp($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpProcessedFolder}/{Path.GetFileName(fileToProcess.Value)}", fileToProcess.Value, clientId);

                                                            if (_movedToProcessedFolder)
                                                            {
                                                                _logger.Info("Copied the file ({}) to the clients processed_folders folder. ClientId={ClientId}", Path.GetFileName(fileToProcess.Value), clientId);
                                                            }
                                                        }

                                                    }
                                                    else
                                                    {
                                                        _logger.Error("Unable to process the file ({}) as a result of the following error ({}). ClientId={ClientId}", fileToProcess, responseBody.Message, clientId);

                                                        if (responseBody.Message.Contains("message"))
                                                        {
                                                            var subject = $"Unable to proceed with the {searchType} Search Request requested by {clientDetails.Name}";
                                                            var header = $"The following error ocurred when processing the {searchType} Search Request requested by {clientDetails.Name}. Please contact Dashro Solutions for further details.";
                                                            int trimLength = responseBody.Message.Length - 14;

                                                            //var result = await email.SendEmailToClient(subject, header, responseBody.Message.Substring(12, trimLength), clientDetails, false);

                                                            //Copy the file to the Pocessed_files Folder.
                                                            _movedToProcessedFolder = ftp.CopyFileToFtp($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpProcessedFolder}/{(responseBody.Message.Substring(12, trimLength).Contains("Searches") ? Error_Types.Requests : Error_Types.Inactive)}_{Path.GetFileName(fileToProcess.Value)}",
                                                                fileToProcess.Value, clientId);

                                                            //Create an Error Text file and add to the to_Client Folder.
                                                            string errorFile = CreateEmailTextFile(responseBody.Message.Substring(12, trimLength), clientId,
                                                                filePaths.Find(f => f.PathName == "SafeSearch").PathFilePath,
                                                                responseBody.Message.Substring(12, trimLength).Contains("Searches") ? Error_Types.Requests : Error_Types.Inactive,
                                                                fileToProcess.Key, itemToProcess);

                                                            if (!string.IsNullOrEmpty(errorFile))
                                                            {
                                                                ftp.CopyFileToFtp($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpClientFolder}/{Path.GetFileName(errorFile)}", errorFile, clientId);
                                                            }
                                                            else
                                                            {
                                                                _logger.Error("Error creating the error text file detailing the error ({}).  ClientId={ClientId}", responseBody.Message.Substring(12, trimLength), clientId);
                                                            }

                                                            //Delete the file from the FTP Folder.
                                                            ftp.DeleteFtpFileFromFolder($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpServerFolder}/{fileToProcess.Key}", clientId);
                                                        }
                                                        else
                                                        {
                                                            //Copy the file to the Pocessed_files Folder.
                                                            _movedToProcessedFolder = ftp.CopyFileToFtp($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpProcessedFolder}/{Error_Types.NoRecords}_{Path.GetFileName(fileToProcess.Value)}", fileToProcess.Value, clientId);

                                                            //Create an Error Text file and add to the to_Client Folder.
                                                            string errorFile = CreateEmailTextFile(responseBody.Message, clientId, filePaths.Find(f => f.PathName == "SafeSearch").PathFilePath,
                                                                Error_Types.NoRecords, fileToProcess.Key, itemToProcess);

                                                            //Copy the error text file to the clients to_client folder.
                                                            if (!string.IsNullOrEmpty(errorFile))
                                                            {
                                                                ftp.CopyFileToFtp($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpClientFolder}/{Path.GetFileName(errorFile)}", errorFile, clientId);
                                                            }

                                                            //Delete the file from the FTP Folder.
                                                            ftp.DeleteFtpFileFromFolder($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpServerFolder}/{fileToProcess.Key}", clientId);


                                                            //Add the details to the log.
                                                            _logger.Error("Unable to process the file ({}) as a result of the following error ({}). ClientId={ClientId}", fileToProcess, responseBody.Message, clientId);

                                                            //Finally delete the error text file.
                                                            if (!string.IsNullOrEmpty(errorFile))
                                                            {
                                                                File.Delete(errorFile);
                                                            }
                                                        }
                                                    }
                                                }
                                                else //Validation Errors
                                                {
                                                    //Copy the file to the Pocessed_files Folder.
                                                    var movedToProfFolder = ftp.CopyFileToFtp($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpProcessedFolder}/{Error_Types.Validation}_{Path.GetFileName(fileToProcess.Value)}", fileToProcess.Value, clientId);

                                                    //Create an Error Text file and add to the to_Client Folder.
                                                    string errorFile = CreateEmailTextFile(validationErrors, clientId, filePaths.Find(f => f.PathName == "SafeSearch").PathFilePath,
                                                        Error_Types.Validation, fileToProcess.Key, itemToProcess);

                                                    if (!string.IsNullOrEmpty(errorFile))
                                                    {
                                                        ftp.CopyFileToFtp($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpClientFolder}/{Path.GetFileName(errorFile)}", errorFile, clientId);
                                                    }

                                                    //Delete the file from the FTP Folder.
                                                    ftp.DeleteFtpFileFromFolder($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpServerFolder}/{fileToProcess.Key}", clientId);

                                                    _logger.Info("Unable to process the file ({}) as a result of the following error ({}). ClientId={ClientId}", fileToProcess, validationErrors, clientId);

                                                }
                                            }
                                            else //Ftp Swift.
                                            {
                                                Swift swift = new Swift(_logger);
                                                Response responseBody = new Response();

                                                //Perform validation on the submitted request and handle any errors by emailing the client of any issues.
                                                var validationErrors = ValidateSwift(itemToProcess, genericFunctions);

                                                if (validationErrors.Length == 0)
                                                {
                                                    string swiftType = itemToProcess.FirstOrDefault(pair => pair.Key == "SheetName").Value;
                                                    System.Threading.Thread.Sleep(10);
                                                    if (swiftType == SwiftTypes.MT101)
                                                    {
                                                        MT101 mt101 = swift.CreateMT101Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt101), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt101, clientDetails, SwiftTypes.MT101, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT103)
                                                    {
                                                        MT103 mt103 = swift.CreateMT103Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt103), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt103, clientDetails, SwiftTypes.MT103, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT192)
                                                    {
                                                        MT192 mt192 = swift.CreateMT192Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt192), clientDetails.ClientId);

                                                        //Send the payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt192, clientDetails, SwiftTypes.MT192, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT199)
                                                    {
                                                        MT199 mt199 = swift.CreateMT199Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt199), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt199, clientDetails, SwiftTypes.MT199, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT200)
                                                    {
                                                        MT200 mt200 = swift.CreateMT200Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt200), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt200, clientDetails, SwiftTypes.MT200, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT202)
                                                    {
                                                        MT202 mt202 = swift.CreateMT202Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt202), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt202, clientDetails, SwiftTypes.MT202, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT210)
                                                    {
                                                        MT210 mt210 = swift.CreateMT210Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt210), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt210, clientDetails, SwiftTypes.MT210, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT299)
                                                    {
                                                        MT299 mt299 = swift.CreateMT299Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt299), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt299, clientDetails, SwiftTypes.MT299, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT380)
                                                    {
                                                        MT380 mt380 = swift.CreateMT380Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt380), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt380, clientDetails, SwiftTypes.MT380, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT399)
                                                    {
                                                        MT399 mt399 = swift.CreateMT399Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt399), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt399, clientDetails, SwiftTypes.MT399, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT499)
                                                    {
                                                        MT499 mt499 = swift.CreateMT499Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt499), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt499, clientDetails, SwiftTypes.MT499, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT502)
                                                    {
                                                        MT502 mt502 = swift.CreateMT502Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt502), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt502, clientDetails, SwiftTypes.MT502, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT540)
                                                    {
                                                        MT540 mt540 = swift.CreateMT540Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt540), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt540, clientDetails, SwiftTypes.MT540, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT541)
                                                    {
                                                        MT541 mt541 = swift.CreateMT541Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt541), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt541, clientDetails, SwiftTypes.MT541, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT542)
                                                    {
                                                        MT542 mt542 = swift.CreateMT542Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt542), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt542, clientDetails, SwiftTypes.MT542, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT543)
                                                    {
                                                        MT543 mt543 = swift.CreateMT543Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt543), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt543, clientDetails, SwiftTypes.MT543, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT545)
                                                    {
                                                        MT545 mt545 = swift.CreateMT545Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt545), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt545, clientDetails, SwiftTypes.MT545, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT547)
                                                    {
                                                        MT547 mt547 = swift.CreateMT547Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt547), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt547, clientDetails, SwiftTypes.MT547, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT548)
                                                    {
                                                        MT548 mt548 = swift.CreateMT548Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt548), clientDetails.ClientId);

                                                        //Send the payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt548, clientDetails, SwiftTypes.MT548, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT565)
                                                    {
                                                        MT565 mt565 = swift.CreateMT565Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt565), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt565, clientDetails, SwiftTypes.MT565, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT599)
                                                    {
                                                        MT599 mt599 = swift.CreateMT599Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt599), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt599, clientDetails, SwiftTypes.MT599, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT699)
                                                    {
                                                        MT699 mt699 = swift.CreateMT699Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt699), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt699, clientDetails, SwiftTypes.MT699, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT799)
                                                    {
                                                        MT799 mt799 = swift.CreateMT799Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt799), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt799, clientDetails, SwiftTypes.MT799, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT899)
                                                    {
                                                        MT899 mt899 = swift.CreateMT899Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt899), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt899, clientDetails, SwiftTypes.MT899, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT920)
                                                    {
                                                        MT920 mt920 = swift.CreateMT920Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt920), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt920, clientDetails, SwiftTypes.MT920, filePaths);
                                                    }
                                                    else if (swiftType == SwiftTypes.MT999)
                                                    {
                                                        MT999 mt999 = swift.CreateMT999Payload(itemToProcess, clientDetails.ClientId, true);
                                                        _logger.Info("Request payload ({}) created. ClientId={ClientId}", json.Serializer(mt999), clientDetails.ClientId);

                                                        //Send the Payload to the endpoint.
                                                        responseBody = await safeSearch.SubmitSwiftRequest(mt999, clientDetails, SwiftTypes.MT999, filePaths);
                                                    }
                                                    if (responseBody.ResponseCode == 200)
                                                    {
                                                        //Fetch the file details of the file to be copied to the client folder.
                                                        string finFileName = await sql.FetchFtpFile(clientId);

                                                        var result = ftp.CopyFileToFtp($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpClientFolder}/{Path.GetFileName(finFileName)}", finFileName, clientId);

                                                        if (result)
                                                        {
                                                            _logger.Info("Copied the file ({}) to the clients to_client folder. ClientId={ClientId}", finFileName, clientId);

                                                            //Mark the file as Processed.
                                                            sql.UpdateFtpFiles(clientId, finFileName);

                                                            //Delete the file from the FTP Folder.
                                                            ftp.DeleteFtpFileFromFolder($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpServerFolder}/{fileToProcess.Key}", clientId);

                                                            _movedToProcessedFolder = ftp.CopyFileToFtp($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpProcessedFolder}/{Path.GetFileName(fileToProcess.Value)}", fileToProcess.Value, clientId);

                                                            //Delete the Zip file.
                                                            if (_movedToProcessedFolder)
                                                            {
                                                                File.Delete(finFileName);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        _logger.Error("Unable to process the file ({}) as a result of the following error ({}). ClientId={ClientId}", fileToProcess, responseBody.Message.Contains("message") ? responseBody.Message.Substring(12, responseBody.Message.Length - 14) : responseBody.Message, clientId);

                                                        //Create an Error Text file and add to the to_Client Folder.
                                                        string errorFile = CreateEmailTextFile(responseBody.Message.Contains("message") ? responseBody.Message.Substring(12, responseBody.Message.Length - 14) : responseBody.Message,
                                                            clientId, filePaths.Find(f => f.PathName == "SafeSearch").PathFilePath, responseBody.Message.Contains("message") ? responseBody.Message.Contains("Swift file creations ") ? Error_Types.Requests : Error_Types.Inactive : Error_Types.NoRecords,
                                                            fileToProcess.Key, itemToProcess);

                                                        if (!string.IsNullOrEmpty(errorFile))
                                                        {
                                                            ftp.CopyFileToFtp($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpClientFolder}/{Path.GetFileName(errorFile)}", errorFile, clientId);
                                                        }

                                                        //Delete the file from the FTP Folder.
                                                        ftp.DeleteFtpFileFromFolder($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpServerFolder}/{fileToProcess.Key}", clientId);

                                                        //Move the file to the Processed folder.
                                                        _movedToProcessedFolder = ftp.CopyFileToFtp($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpProcessedFolder}/{Path.GetFileName(fileToProcess.Value)}", fileToProcess.Value, clientId);

                                                        //Delete the error file only if copied successfully.
                                                        if (_movedToProcessedFolder)
                                                        {
                                                            File.Delete(errorFile);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    _logger.Error("Unable to process the file ({}) as a result of the following error ({}). ClientId={ClientId}", fileToProcess, validationErrors, clientId);

                                                    //Create an Error Text file and add to the to_Client Folder.
                                                    string errorFile = CreateEmailTextFile(validationErrors, clientId, filePaths.Find(f => f.PathName == "SafeSearch").PathFilePath,
                                                        Error_Types.Validation, fileToProcess.Key, itemToProcess);

                                                    if (!string.IsNullOrEmpty(errorFile))
                                                    {
                                                        ftp.CopyFileToFtp($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpClientFolder}/{Path.GetFileName(errorFile)}", errorFile, clientId);
                                                    }

                                                    //Delete the file from the FTP Folder.
                                                    ftp.DeleteFtpFileFromFolder($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpServerFolder}/{fileToProcess.Key}", clientId);

                                                    //Move the file to the Processed folder.
                                                    _movedToProcessedFolder = ftp.CopyFileToFtp($"{Properties.Settings.Default.FtpUrl}/{clientId}/{Properties.Settings.Default.FtpProcessedFolder}/{Path.GetFileName(fileToProcess.Value)}", fileToProcess.Value, clientId);

                                                    //Delete the error file only if copied successfully.
                                                    if (_movedToProcessedFolder)
                                                    {
                                                        File.Delete(errorFile);
                                                    }

                                                }
                                            }
                                        }
                                    }

                                    ///Clear out the files from the Extracts folder.
                                    genericFunctions.ClearOutExtractFolder(FilesToProcess);
                                }
                                else
                                {
                                    _logger.Info("Completed checking, no FTP files found. ClientId={ClientId}", clientId);
                                }
                            }
                        }
                    }
                    else
                    {
                        _logger.Info($"Completed checking, no FTP clients found");
                    }
                }
                catch (Exception ex)
                {
                    _logger.Error($"Error extracting the values from the ftp | Error message ({ex.Message}).");
                }

                _logger.Info($"Completed processing of ftp files.");
            }
            catch (Exception ex)
            {
                _logger.Error($"Error extracting the values from the file/ftp | Error message ({ex.Message}).");
            }
        }

        //Was Private Static!
        private static string CreateEmailTextFile(string errorMessage, string clientId, string filePath, 
            string errorType, string processFileName, Dictionary<string, string> requestParams)
        {
            string folder = $"{filePath}{Properties.Settings.Default.ErrorFolder}\\{clientId}";
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }

            string fileName = $"{folder}\\{errorType}_{clientId} _{Path.GetFileNameWithoutExtension(processFileName)}_{String.Format("{0:ddMMyyyy_HHmm}", DateTime.Now)}.txt";

            using (FileStream fs = File.Create(fileName))
            {
                // Add some text to file    
                Byte[] title = new UTF8Encoding(true).GetBytes($"Error processing the file {processFileName} " + Environment.NewLine);
                fs.Write(title, 0, title.Length);
                byte[] body = new UTF8Encoding(true).GetBytes($"Error message - {errorMessage}" + Environment.NewLine);
                fs.Write(body, 0, body.Length);
                byte[] head = new UTF8Encoding(true).GetBytes("Request Parameters Listed Below;" + Environment.NewLine);
                fs.Write(head, 0, head.Length);
                foreach (KeyValuePair<string, string> requestParam in requestParams.Where(r=> r.Key != "SheetName"))
                {
                    byte[] param = new UTF8Encoding(true).GetBytes($"Request Parameter - ({requestParam.Key}); Request Value - ({requestParam.Value})" + Environment.NewLine);
                    fs.Write(param, 0, param.Length);
                }
            }

            _logger.Info("Error file ({}) copied to the folder ({}) detailing the problems found whilst processing.  ClientId={ClientId}", Path.GetFileName(fileName), Properties.Settings.Default.FtpClientFolder, clientId);

            return fileName;
        }

        /// <summary>
        /// Perform validation on the request.
        /// </summary>
        /// <param name="requestParams"></param>
        /// <param name="searchType"></param>
        /// <returns></returns>
        private string ValidateRequest(Dictionary<string, string> requestParams, string searchType, bool ftp)
        {
            var errors = new StringBuilder();

            if (string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "Threshold").Value))
            {
                errors.AppendLine("Threshold cannot be null or missing.");
            }

            if (searchType=="Person") 
            {
                if (string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "Surname").Value))
                {

                    errors.AppendLine("Surname cannot be empty for a person Request.");
                }

                if (requestParams.FirstOrDefault(pair => pair.Key == "Date of Birth").Value != null)
                {
                    if (DateTime.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "Date of Birth").Value, out DateTime tempTo) == false)
                    {
                       errors.AppendLine($"Invalid format for Date of birth ({requestParams.FirstOrDefault(pair => pair.Key == "Date of Birth").Value}), this must be yyyy-mm-dd and be a valid date");
                    }
                }

                if (!string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "Year of Birth").Value))
                {
                    if (!requestParams.FirstOrDefault(pair => pair.Key == "Year of Birth").Value.ToString().All(char.IsNumber))
                    {
                           errors.AppendLine($"Year of birth ({requestParams.FirstOrDefault(pair => pair.Key == "Year of Birth").Value}) must be a numeric value.");
                    }
                }

                if (requestParams.FirstOrDefault(pair => pair.Key == "Date of Death").Value != null)
                {
                    if (DateTime.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "Date of Death").Value, out DateTime tempTo) == false)
                    {
                           errors.AppendLine($"Invalid format for Date of death ({requestParams.FirstOrDefault(pair => pair.Key == "Date of Death").Value}), this must be yyyy-mm-dd and be a valid date.");
                    }
                }

                if (!string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "Year of Death").Value))
                {
                    if (!requestParams.FirstOrDefault(pair => pair.Key == "Year of Death").Value.ToString().All(char.IsNumber))
                    {
                         errors.AppendLine($"Year of death ({requestParams.FirstOrDefault(pair => pair.Key == "Year of Death").Value}) must be a numeric value.");
                    }
                }

            }
            else 
            {
                if (string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "Business Name").Value))
                {
                    errors.AppendLine("Business Name cannot be empty for a business/bank Request.");
                }
            }

            if (!ftp)
            {
                if (string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "Email").Value))
                {
                    errors.AppendLine("A valid email address is required in order to send the searh request result");
                }
                else if (!string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "Email").Value))
                {
                    string emailsArray = requestParams.FirstOrDefault(pair => pair.Key == "Email").Value;
                    string[] emails = emailsArray.Split(';');
                    foreach (string email in emails)
                    {
                        if (!new EmailAddressAttribute().IsValid(email))
                        {
                            errors.AppendLine($"Invalid email address ({email}) used.");
                        }
                    }
                }
            }

            return errors.ToString();
        }

        private string ValidateSwift(Dictionary<string, string> requestParams, GenericFunctions genericFunctions)
        {
            var errors = new StringBuilder();

            try  //Generic Validation - this is fields that occur in all Swift Files.
            {
                #region Generic

                //Client Swift Code.
                if (requestParams.ContainsKey("Client Swift Code"))
                {
                    if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value))
                    {
                        errors.AppendLine($"Client swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Client Swift Code").Value}) appears to be incorrectly formatted.");
                    }
                }
                else
                {
                    errors.AppendLine("Client swift code cannot be null or missing.");
                }

                //Order Ref.
                if (!requestParams.ContainsKey("Order Ref"))
                {
                    errors.AppendLine("Order ref cannot be null or missing.");
                }

                //Not applicable to MT192, MTn99, MT502, MT565 & MT920
                if (requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value != "MT192" && 
                    requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value != "MT502" &&
                    requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value != "MT920" &&
                    requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value != "MT565" &&
                    requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value.Substring(3, 2) != "99" )
                {
                    //Settlement Date
                    if (!requestParams.ContainsKey("Settlement Date"))
                    {
                        errors.AppendLine("Settlement Date cannot be null or missing.");
                    }
                    else
                    {
                        if (DateTime.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "Settlement Date").Value, out DateTime tempTo) == false)
                        {
                            errors.AppendLine($"Invalid format for Settlement Date ({requestParams.FirstOrDefault(pair => pair.Key == "Settlement Date").Value}), this must be yyyy-mm-dd and be a valid date");
                        }
                    }

                    //Settlement Amount.
                    if (!requestParams.ContainsKey("Settlement Amount"))
                    {
                        errors.AppendLine("Settlement amount cannot be null or missing.");
                    }
                    else
                    {
                        if (!genericFunctions.IsNumericFromTryParse(requestParams.FirstOrDefault(pair => pair.Key == "Settlement Amount").Value.ToString()))
                        {
                            errors.AppendLine($"Settlement ammount ({requestParams.FirstOrDefault(pair => pair.Key == "Settlement Amount").Value}) must contain numeric values only.");
                        }
                    }
                }

                //Email.
                if (requestParams.ContainsKey("Email"))
                {

                    string[] emails = requestParams.FirstOrDefault(pair => pair.Key == "Email").Value.Split(';');
                    foreach (string email in emails)
                    {
                        if (!new EmailAddressAttribute().IsValid(email))
                        {
                            errors.AppendLine($"Invalid email address {email} used.");
                        }
                    }
                }
                #endregion
                #region MT101
                if (requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value == "MT101") //MT101
                {
                    //Order Ref.
                    if (!requestParams.ContainsKey("Order Name"))
                    {
                        errors.AppendLine("Order name cannot be null or missing.");
                    }

                    //Order Swift.
                    if (requestParams.ContainsKey("Order Swift"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Order Swift").Value))
                        {
                            errors.AppendLine($"Order swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Order Swift").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Order swift code cannot be null or missing.");
                    }

                    //Order Iban.
                    if (requestParams.ContainsKey("Order IBAN"))
                    {
                        if (!genericFunctions.ValidIBAN(requestParams.FirstOrDefault(pair => pair.Key == "Order IBAN").Value))
                        {
                            errors.AppendLine($"Order IBAN ({requestParams.FirstOrDefault(pair => pair.Key == "Order IBAN").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Order IBAN cannot be null or missing.");
                    }

                    //Beneficiary Bank Swift and IBAN.
                    if (requestParams.ContainsKey("Beneficiary Bank Name"))
                    {
                        if(!requestParams.ContainsKey("Beneficiary IBAN") && !requestParams.ContainsKey("Beneficiary Bank IBAN") &&  !requestParams.ContainsKey("Beneficiary Name"))
                        {
                            errors.AppendLine("Either a Beneficiary IBAN, Beneficiary Bank IBAN or Beneficiary Name must be supplied.");
                        }

                        //Beneficiary Swift.
                        if (requestParams.ContainsKey("Beneficiary Swift"))
                        {
                            if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Beneficiary Swift").Value))
                            {
                                errors.AppendLine($"Beneficiary swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Beneficiary Swift").Value}) appears to be incorrectly formatted.");
                            }
                        }

                        //Beneficiary IBAN.
                        if (requestParams.ContainsKey("Beneficiary IBAN"))
                        {
                            if (!genericFunctions.ValidIBAN(requestParams.FirstOrDefault(pair => pair.Key == "Beneficiary IBAN").Value))
                            {
                                errors.AppendLine($"Beneficiary IBAN ({requestParams.FirstOrDefault(pair => pair.Key == "Beneficiary IBAN").Value}) appears to be incorrectly formatted.");
                            }
                        }

                        //Beneficiary Bank IBAN.
                        if (requestParams.ContainsKey("Beneficiary Bank IBAN"))
                        {
                            if (!genericFunctions.ValidIBAN(requestParams.FirstOrDefault(pair => pair.Key == "Beneficiary Bank IBAN").Value))
                            {
                                errors.AppendLine($"Beneficiary Bank IBAN ({requestParams.FirstOrDefault(pair => pair.Key == "Beneficiary Bank IBAN").Value}) appears to be incorrectly formatted.");
                            }
                        }

                        //Beneficiary Bank Swift.
                        if (requestParams.ContainsKey("Beneficiary Bank Swift"))
                        {
                            if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Beneficiary Bank Swift").Value))
                            {
                                errors.AppendLine($"Beneficiary Bank swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Beneficiary Bank Swift").Value}) appears to be incorrectly formatted.");
                            }
                        }
                    }
                    else
                    {
                        errors.AppendLine("Beneficiary bank name cannot be null or missing.");
                    }

                    //Beneficiary Sub Bank Swfit and IBAN
                    //if (requestParams.ContainsKey("BeneficiarySubBankName"))
                    //{
                    //Beneficiary Sub Bank Swift.
                    if (requestParams.ContainsKey("Beneficiary Sub Bank Swift"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Beneficiary Sub Bank Swift").Value))
                        {
                            errors.AppendLine($"Beneficiary sub bank swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Beneficiary Sub Bank Swift").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    //else
                    //{
                    //    errors.AppendLine("Beneficiary sub bank swift code cannot be null or missing when the Beneficiary Sub Bank Name has been supplied.");
                    //}

                        //Beneficiary Sub Bank IBAN.
                        //if (requestParams.ContainsKey("BeneficiarySubBankIBAN"))
                        //{
                        //    if (!genericFunctions.ValidIBAN(requestParams.FirstOrDefault(pair => pair.Key == "BeneficiarySubBankIBAN").Value))
                        //    {
                        //        errors.AppendLine($"Beneficiary sub bank iban ({requestParams.FirstOrDefault(pair => pair.Key == "BeneficiarySubBankIBAN").Value}) appears to be incorrectly formatted.");
                        //    }
                        //}
                        //else
                        //{
                        //    errors.AppendLine("Beneficiary sub bank iban cannot be null or missing when the Beneficiary Sub Bank Name has been supplied.");
                        //}
                    //}

                }
                #endregion
                #region MT103
                else if (requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value == "MT103") //MT103
                {
                    //Order Swift.
                    if (requestParams.ContainsKey("Order Swift"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Order Swift").Value))
                        {
                            errors.AppendLine($"Order swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Order Swift").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Order swift code cannot be null or missing.");
                    }

                    //Order Name.
                    if (!requestParams.ContainsKey("Order Name"))
                    {
                        errors.AppendLine("Order name cannot be null or missing.");
                    }

                    //Order Address.
                    if (!requestParams.ContainsKey("Order Address"))
                    {
                        errors.AppendLine("Order address cannot be null or missing.");
                    }

                    //Order Iban/Account number
                    if (!requestParams.ContainsKey("Order IBAN/AccountNumber"))
                    {
                        errors.AppendLine("Order IBAN/Account Number cannot be null or missing.");
                    }

                    //Beneficiary IBAN/AccountNumber
                    if (!requestParams.ContainsKey("Beneficiary IBAN/AccountNumber") && !requestParams.ContainsKey("Beneficiary Bank IBAN/Account Number"))
                    {
                        errors.AppendLine("Beneficiary IBAN/AccountNumber & Beneficiary Bank IBAN/Account Number cannot be null or missing.");
                    }

                    //Beneficiary Swift Code & Beneficiary Bank Swift Code.
                    if (!requestParams.ContainsKey("Beneficiary Swift") && !requestParams.ContainsKey("Beneficiary Bank Swift"))
                    {
                        errors.AppendLine("Beneficiary Swift & Beneficiary Bank Swift cannot be null or missing.");
                    }
                    else
                    {
                        if (!requestParams.ContainsKey("Beneficiary Swift")  && requestParams.ContainsKey("Beneficiary Bank Swift"))
                        {
                            if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Beneficiary Swift").Value))
                            {
                                errors.AppendLine($"Beneficiary Swift ({requestParams.FirstOrDefault(pair => pair.Key == "Beneficiary Swift").Value}) appears to be incorrectly formatted.");
                            }
                               
                        }
                        else
                        {
                            if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Beneficiary Swift").Value))
                            {
                                errors.AppendLine($"Beneficiary Swift ({requestParams.FirstOrDefault(pair => pair.Key == "Beneficiary Swift").Value}) appears to be incorrectly formatted.");
                            }

                        }
                    }

                    //Beneficiary Sub Bank Swift.
                    if (requestParams.ContainsKey("Beneficiary Sub Bank Swift"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Beneficiary Sub Bank Swift").Value))
                        {
                            errors.AppendLine($"Beneficiary sub bank swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Beneficiary Sub Bank Swift").Value}) appears to be incorrectly formatted.");
                        }
                    }

                }
                #endregion
                #region MT192
                else if (requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value == "MT192") //MT192
                {
                    //Order Related Ref.
                    if (!requestParams.ContainsKey("Order Related Ref"))
                    {
                        string msg = "Order Related Ref cannot be null or missing.";
                        return msg;
                    }

                    //Order Related Date
                    if (!requestParams.ContainsKey("Order Related Date"))
                    {
                        errors.AppendLine("Order Related Date cannot be null or missing.");
                    }
                    else
                    {
                        if (DateTime.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "Order Related Date").Value, out DateTime tempTo) == false)
                        {
                            errors.AppendLine($"Invalid format for Order Related Date ({requestParams.FirstOrDefault(pair => pair.Key == "Order Related Date").Value}), this must be yyyy-mm-dd and be a valid date");
                        }
                    }

                    //Order Swift Code.
                    if (requestParams.ContainsKey("Order Swift Code"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value))
                        {
                            errors.AppendLine($"Order swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Order swift code cannot be null or missing.");
                    }

                }
                #endregion
                #region MT199/MT299/MT399
                else if (requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value.Substring(3, 2) == "99") 
                {
                    //Broker Swift Code.
                    if (requestParams.ContainsKey("Order Swift Code"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value))
                        {
                            errors.AppendLine($"Order swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Order swift code cannot be null or missing.");
                    }

                    //Message.
                    if (!requestParams.ContainsKey("Message"))
                    {
                        errors.AppendLine("Message cannot be null or missing.");
                    }

                }
                #endregion
                #region MT200
                else if (requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value == "MT200") //MT200
                {
                    //Benficiary Iban.
                    if (requestParams.ContainsKey("Beneficiary IBAN"))
                    {
                        if (!genericFunctions.ValidIBAN(requestParams.FirstOrDefault(pair => pair.Key == "Beneficiary IBAN").Value))
                        {
                            errors.AppendLine($"Benficiary IBAN ({requestParams.FirstOrDefault(pair => pair.Key == "Beneficiary IBAN").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Benficiary IBAN cannot be null or missing.");
                    }

                    //Order Iban.
                    if (requestParams.ContainsKey("Order IBAN"))
                    {
                        if (!genericFunctions.ValidIBAN(requestParams.FirstOrDefault(pair => pair.Key == "Order IBAN").Value))
                        {
                            errors.AppendLine($"Order IBAN ({requestParams.FirstOrDefault(pair => pair.Key == "Order IBAN").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Order IBAN cannot be null or missing.");
                    }

                    //Order Swift Code.
                    if (requestParams.ContainsKey("Order Swift Code"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value))
                        {
                            errors.AppendLine($"Order swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Order swift code cannot be null or missing.");
                    }
                }
                #endregion
                #region MT202
                else if (requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value == "MT202")
                {
                    //Order Name.                 
                    if (!requestParams.ContainsKey("Order Name"))
                    {
                        errors.AppendLine("Order name cannot be null or missing.");
                    }


                    //Order Address.
                    if (!requestParams.ContainsKey("Order Address"))
                    {
                        errors.AppendLine("Order address cannot be null or missing.");
                    }

                    //Order Swift.
                    if (requestParams.ContainsKey("Order Swift"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Order Swift").Value))
                        {
                            errors.AppendLine($"Order swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Order Swift").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Order swift code cannot be null or missing.");
                    }

                    //Order Account Number.
                    if (!requestParams.ContainsKey("OrderAccountNumber"))
                    {
                        errors.AppendLine($"Order account number cannot be null or missing.");
                    }
                    else
                    {
                        if (!requestParams.FirstOrDefault(pair => pair.Key == "OrderAccountNumber").Value.ToString().All(char.IsNumber))
                        {
                            errors.AppendLine("Order account number must contain numeric values only.");
                        }
                    }

                    //Order Iban.
                    if (requestParams.ContainsKey("OrderIban"))
                    {
                        if (!genericFunctions.ValidIBAN(requestParams.FirstOrDefault(pair => pair.Key == "OrderIban").Value))
                        {
                            errors.AppendLine($"Order IBAN ({requestParams.FirstOrDefault(pair => pair.Key == "OrderIban").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Order IBAN cannot be null or missing.");
                    }

                    //Beneficiary Ref.
                    if (!requestParams.ContainsKey("Beneficiary Ref"))
                    {
                        errors.AppendLine("Beneficiary Ref cannot be null or missing.");
                    }

                    //Beneficiary Name.
                    if (!requestParams.ContainsKey("Beneficiary Name"))
                    {
                        errors.AppendLine("Beneficiary Name cannot be null or missing.");
                    }

                    //Beneficiary Swift
                    if (requestParams.ContainsKey("Beneficiary Swift"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Beneficiary Swift").Value))
                        {
                            errors.AppendLine($"Beneficiary swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Beneficiary Swift").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Beneficiary swift code cannot be null or missing.");
                    }

                    //Beneficiary Account Number.
                    if (requestParams.ContainsKey("BeneficiaryAccountNumber"))
                    {
                        if (!requestParams.FirstOrDefault(pair => pair.Key == "BeneficiaryAccountNumber").Value.ToString().All(char.IsNumber))
                        {
                            errors.AppendLine("Beneficiary account number must contain numeric values only.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Beneficiary account number cannot be null or missing.");
                    }

                    //TODO! - Will be uncommented for Live - Important!
                    //Beneficiary IBAN.
                    //if (requestParams.ContainsKey("BeneficiaryIBAN"))
                    //{
                    //    if (!genericFunctions.ValidIBAN(requestParams.FirstOrDefault(pair => pair.Key == "BeneficiaryIBAN").Value))
                    //    {
                    //        var msg = $"Beneficiary IBAN ({requestParams.FirstOrDefault(pair => pair.Key == "BeneficiaryIBAN").Value}) appears to be incorrectly formatted.";
                    //        return msg;
                    //    }
                    //}
                    //else
                    //{
                    //    errors.AppendLine("Beneficiary IBAN cannot be null or missing.");
                    //}


                    //TODO! - Beneficiary Sub Bank Swift and Beneficiary Bank IBAN to be added later.
                    //Beneficiary Sub Bank Swift.
                    //if (requestParams.ContainsKey("BeneficiarySubBankSwift"))
                    //{
                    //    if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "BeneficiarySubBankSwift").Value))
                    //    {
                    //        errors.AppendLine($"Beneficiary sub bank swift code ({requestParams.FirstOrDefault(pair => pair.Key == "BeneficiarySubBankSwift").Value}) appears to be incorrectly formatted.");
                    //    }
                    //}
                    //else
                    //{
                    //    errors.AppendLine("Beneficiary sub bank swift code cannot be null or missing.");
                    //}

                    //Beneficiary Bank IBAN.
                    //if (requestParams.ContainsKey("BeneficiaryBankIBAN"))
                    //{
                    //    if (!genericFunctions.ValidIBAN(requestParams.FirstOrDefault(pair => pair.Key == "BeneficiaryBankIBAN").Value))
                    //    {
                    //        errors.AppendLine($"Beneficiary bank IBAN ({requestParams.FirstOrDefault(pair => pair.Key == "BeneficiaryBankIBAN").Value}) appears to be incorrectly formatted.");
                    //    }
                    //}
                    //else
                    //{
                    //    errors.AppendLine("Beneficiary bank IBAN cannot be null or missing.");
                    //}
                   
                }
                #endregion
                #region MT210
                else if (requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value == "MT210")
                {
                    //Beneficiary Swift
                    if (requestParams.ContainsKey("BeneficiarySwiftCode"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "BeneficiarySwiftCode").Value))
                        {
                            errors.AppendLine($"Beneficiary swift code ({requestParams.FirstOrDefault(pair => pair.Key == "BeneficiarySwiftCode").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Beneficiary swift code cannot be null or missing.");
                    }

                    //Related Order Ref
                    if (!requestParams.ContainsKey("RelatedOrderRef"))
                    {
                        errors.AppendLine("Related order ref code cannot be null or missing.");
                    }

                    //Order Swift
                    if (requestParams.ContainsKey("OrderSwiftCode"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "OrderSwiftCode").Value))
                        {
                            errors.AppendLine($"Order swift code ({requestParams.FirstOrDefault(pair => pair.Key == "OrderSwiftCode").Value}) appears to be incorrectly formatted.");
                        }
                    }

                    //Beneficiary Sub Bank Swift
                    if (requestParams.ContainsKey("BeneficiarySubBankSwift"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "BeneficiarySubBankSwift").Value))
                        {
                            errors.AppendLine($"Beneficiary sub bank swift code ({requestParams.FirstOrDefault(pair => pair.Key == "BeneficiarySubBankSwift").Value}) appears to be incorrectly formatted.");
                        }
                    }
                }
                #endregion
                #region MT380
                else if (requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value == "MT380")
                {
                    //Broker Swift Code.
                    if (requestParams.ContainsKey("BrokerSwiftCode"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "BrokerSwiftCode").Value))
                        {
                            errors.AppendLine($"Broker swift code ({requestParams.FirstOrDefault(pair => pair.Key == "BrokerSwiftCode").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Broker swift code cannot be null or missing.");
                    }

                    //Safekeeping Account.
                    if (!requestParams.ContainsKey("SafeKeepingAccount"))
                    {
                        errors.AppendLine("SafeKeeping Account cannot be null or missing.");
                    }
                }
                #endregion
                #region MT502
                else if (requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value == "MT502")
                {
                    //Cancel Order Ref
                    if (requestParams.FirstOrDefault(pair => pair.Key == "TradeType").Value != "NEWM")
                    {
                        if (!requestParams.ContainsKey("CancelOrderRef"))
                        {
                            var msg = "Cancel order ref cannot be null or missing.";
                            return msg;
                        }
                    }

                    //Broker Account.
                    if (!requestParams.ContainsKey("BrokerAccountNumber"))
                    {
                        errors.AppendLine("Broker Account cannot be null or missing.");
                    }

                    //Quantity/Amount.
                    if (Convert.ToDouble(requestParams.FirstOrDefault(pair => pair.Key == "Quantity").Value) > 0)
                    {
                        if (string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "Quantity").Value))
                        {
                            var msg = "Quantity cannot be null or missing.";
                            return msg;
                        }
                        else
                        {
                            double valPerc;
                            bool parsedPerc = double.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "Quantity").Value, out valPerc);
                            if (!parsedPerc)
                            {
                                errors.AppendLine($"Quantity ({requestParams.FirstOrDefault(pair => pair.Key == "Quantity").Value}) must contain numeric values only.");
                            }
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "Amount").Value))
                        {
                            var msg = "Amount cannot be null or missing.";
                            return msg;
                        }
                        else
                        {
                            double valAmnt;
                            bool parsed = double.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "Amount").Value, out valAmnt);
                            if (!parsed)
                            {
                                var msg = $"Amount ({requestParams.FirstOrDefault(pair => pair.Key == "Amount").Value}) must contain numeric values only.";
                                return msg;
                            }
                        }
                    }

                    //Instrument ISIN.
                    if (!requestParams.ContainsKey("InstrumentISIN"))
                    {
                        errors.AppendLine("Instrument ISIN cannot be null or missing.");
                    }
                }
                #endregion
                #region MT540
                else if (requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value == "MT540")
                {
                    //Broker Swift Code.
                    if (requestParams.ContainsKey("Broker Swift Code"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Broker Swift Code").Value))
                        {
                            errors.AppendLine($"Broker swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Broker Swift Code").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Order swift code cannot be null or missing.");
                    }

                    //Cancel Order Ref
                    if (requestParams.FirstOrDefault(pair => pair.Key == "Trade Type").Value != "NEWM")
                    {
                        if (!requestParams.ContainsKey("Cancel Order Ref"))
                        {
                            var msg = "Cancel order ref cannot be null or missing when the trade type is CANC.";
                            return msg;
                        }
                    }

                    //Broker Account Number.
                    if (!requestParams.ContainsKey("Broker Account Number"))
                    {
                        errors.AppendLine("Broker Account cannot be null or missing.");
                    }

                    //Trade Date.
                    if (!string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "Trade Date").Value))
                    {
                        if (DateTime.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "Trade Date").Value, out DateTime tempTo) == false)
                        {
                            errors.AppendLine($"Invalid format for Trade Date ({requestParams.FirstOrDefault(pair => pair.Key == "Trade Date").Value}), this must be yyyy-mm-dd and be a valid date");
                        }
                    }

                    //Settlement Date.
                    if (!string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "Settlement Date").Value))
                    {
                        if (DateTime.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "Settlement Date").Value, out DateTime tempTo) == false)
                        {
                            errors.AppendLine($"Invalid format for Settlement Date ({requestParams.FirstOrDefault(pair => pair.Key == "Settlement Date").Value}), this must be yyyy-mm-dd and be a valid date");
                        }
                    }

                    //Instrument ISIN.
                    if (!requestParams.ContainsKey("Instrument ISIN"))
                    {
                        errors.AppendLine("Instrument ISIN cannot be null or missing.");
                    }

                    //Delivering Agent Swift Code.
                    if (requestParams.ContainsKey("Delivering Agent Swift Code"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Delivering Agent Swift Code").Value))
                        {
                            errors.AppendLine($"Delivering Agent swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Delivering Agent Swift Code").Value}) appears to be incorrectly formatted.");
                        }
                        else
                        {
                            if (!requestParams.ContainsKey("Delivering Agent"))
                            {
                                var msg = "Delivering Agent cannot be null or missing when a Delivering Agent Swift Code is present.";
                                return msg;
                            }
                        }
                    }
                    else
                    {
                        errors.AppendLine("Delivering Agent swift code cannot be null or missing.");
                    }

                    //Delivering Agent Swift Code.
                    if (requestParams.ContainsKey("Settlement Swift Code"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Settlement Swift Code").Value))
                        {
                            errors.AppendLine($"Settlement swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Settlement Swift Code").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Settlement swift code cannot be null or missing.");
                    }
                }
                #endregion
                #region MT541
                //Swift Specific Validation.
                if (requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value == "MT541") //MT541
                {
                    //Broker Swift Code.
                    if (requestParams.ContainsKey("BrokerSwiftCode"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "BrokerSwiftCode").Value))
                        {
                            errors.AppendLine($"Broker swift code ({requestParams.FirstOrDefault(pair => pair.Key == "BrokerSwiftCode").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Broker swift code cannot be null or missing.");
                    }

                    //Preparation Date.
                    if (!string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "PreparationDate").Value))
                    {
                        if (DateTime.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "PreparationDate").Value, out DateTime tempTo) == false)
                        {
                            errors.AppendLine($"Invalid format for Preperation Date ({requestParams.FirstOrDefault(pair => pair.Key == "PreparationDate").Value}), this must be yyyy-mm-dd and be a valid date");
                        }
                    }

                    //Trade Date.
                    if (!string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "TradeDate").Value))
                    {
                        if (DateTime.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "TradeDate").Value, out DateTime tempTo) == false)
                        {
                            errors.AppendLine($"Invalid format for Trade Date ({requestParams.FirstOrDefault(pair => pair.Key == "TradeDate").Value}), this must be yyyy-mm-dd and be a valid date");
                        }
                    }

                    //DealPrice - DealPerc
                    if (!requestParams.ContainsKey("DealPrice") &&
                        !requestParams.ContainsKey("DealPrice%"))
                    {
                        errors.AppendLine("Either a Deal Price or Deal Price% must be entered");
                    }

                    if (!string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "DealPrice").Value)
                        && string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "DealPrice%").Value))
                    {
                        double val;
                        bool parsed = double.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "DealPrice").Value, out val);
                        if (!parsed)
                        {
                            errors.AppendLine($"Deal Price ({requestParams.FirstOrDefault(pair => pair.Key == "DealPrice").Value}) must contain numeric values only.");
                        }
                    }
                    else if (string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "DealPrice").Value)
                        && !string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "DealPrice%").Value))
                    {
                        double valPerc;
                        bool parsedPerc = double.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "DealPrice%").Value, out valPerc);
                        if (!parsedPerc)
                        {
                            errors.AppendLine($"Deal Price% ({requestParams.FirstOrDefault(pair => pair.Key == "DealPrice%").Value}) must contain numeric values only.");
                        }
                    }

                    //Quantity
                    if (!requestParams.ContainsKey("Quantity"))
                    {
                        errors.AppendLine($"Quantity cannot be null or missing.");
                    }
                    else
                    {
                        if (!requestParams.FirstOrDefault(pair => pair.Key == "Quantity").Value.ToString().All(char.IsNumber))
                        {
                            errors.AppendLine($"Quantity ({requestParams.FirstOrDefault(pair => pair.Key == "Quantity").Value}) must contain numeric values only.");
                        }
                    }

                    //Safe Keeping Account.
                    if (string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "SafeKeepingAccount").Value.Trim()))
                    {
                        errors.AppendLine("Safe Keeping Account must be entered.");
                    }

                    //Custodian Swift Code.
                    if (requestParams.ContainsKey("CustodianSwiftCode"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "CustodianSwiftCode").Value.Trim()))
                        {
                            errors.AppendLine($"Custodian swift code ({requestParams.FirstOrDefault(pair => pair.Key == "CustodianSwiftCode").Value}) appears to be incorrectly formatted.");
                        }
                    }

                    //Seller Swift Code - Seller Name.
                    //if (!requestParams.ContainsKey("SellerSwiftCode") && !requestParams.ContainsKey("SellerName"))
                    //{                     
                    //    errors.AppendLine($"Either a Seller Swift Code or Seller Name is required.");
                    //}
                    //else if (requestParams.ContainsKey("SellerSwiftCode"))
                    //{
                    //    if (!string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "SellerSwiftCode").Value.Trim()))
                    //    {
                    //        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "SellerSwiftCode").Value.Trim()))
                    //        {
                    //            errors.AppendLine($"Seller swift code ({requestParams.FirstOrDefault(pair => pair.Key == "SellerSwiftCode").Value}) appears to be incorrectly formatted.");
                    //        }
                    //    }
                    //}

                    //Buyer/Seller Swift Code.
                    if ((!requestParams.ContainsKey("BuyerSwiftCode") && !requestParams.ContainsKey("BuyerName")) &&
                        (!requestParams.ContainsKey("SellerSwiftCode") && !requestParams.ContainsKey("SellerName")))
                    {
                        errors.AppendLine("Either a Buyer swift code, Buyer Name or Seller Swift Code, Seller Name are required.");
                    }
                    else if (requestParams.ContainsKey("BuyerSwiftCode"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "BuyerSwiftCode").Value.Trim()))
                        {
                            errors.AppendLine($"Buyer swift code ({requestParams.FirstOrDefault(pair => pair.Key == "BuyerSwiftCode").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else if (requestParams.ContainsKey("SellerSwiftCode"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "SellerSwiftCode").Value.Trim()))
                        {
                            errors.AppendLine($"Seller swift code ({requestParams.FirstOrDefault(pair => pair.Key == "SellerSwiftCode").Value}) appears to be incorrectly formatted.");
                        }
                    }

                    //Settlement Swift Code.
                    if (!requestParams.ContainsKey("SettlementSwiftCode"))
                    {
                        errors.AppendLine($"Settlement Swift Code cannot be null or missing.");
                    }
                    else if (!string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "SettlementSwiftCode").Value.Trim()))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "SettlementSwiftCode").Value.Trim()))
                        {
                            errors.AppendLine($"Settlement swift code ({requestParams.FirstOrDefault(pair => pair.Key == "SettlementSwiftCode").Value}) appears to be incorrectly formatted.");
                        }
                    }

                    //Settlement Transaction Indicator.
                    if (!requestParams.ContainsKey("SettlementTransactionIndicator"))
                    {
                        errors.AppendLine($"Settlement Transaction Indicator cannot be null or missing.");
                    }

                    //Instrument ISIN.
                    if (!requestParams.ContainsKey("InstrumentISIN"))
                    {
                        errors.AppendLine($"Instrument ISIN cannot be null or missing.");
                    }

                    //Settlement Party Name
                    if (!requestParams.ContainsKey("SettlementPartyName") && !requestParams.ContainsKey("DeliveringAgentCode"))
                    {
                        errors.AppendLine("Settlement Party Name and/or Delivering Agent Name cannot be null or missing.");
                    }
                    else if (requestParams.ContainsKey("SettlementPartyName") &&
                       !requestParams.ContainsKey("DeliveringAgentCode"))
                    {
                        errors.AppendLine("If the Settlement Party Name is set then the Delivering Agent must also be entered.");
                    }

                    //Settlement Ammount.
                    if (!requestParams.ContainsKey("SettlementAmount"))
                    {
                        errors.AppendLine($"Settlement Amount cannot be null or missing.");
                    }
                    else
                    {
                        double valPerc;
                        bool parsedPerc = double.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "SettlementAmount").Value, out valPerc);
                        if (!parsedPerc)
                        {
                            errors.AppendLine("Settlement Amount must contain numeric values only.");
                        }
                    }
                }
                #endregion
                #region MT542
                else if (requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value == "MT542")
                {
                    //Broker Swift Code.
                    if (requestParams.ContainsKey("Broker Swift Code"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Broker Swift Code").Value))
                        {
                            errors.AppendLine($"Broker swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Broker Swift Code").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Broker swift code cannot be null or missing.");
                    }

                    //Cancel Order Ref
                    if (requestParams.FirstOrDefault(pair => pair.Key == "Trade Type").Value != "NEWM")
                    {
                        if (!requestParams.ContainsKey("Cancel Order Ref"))
                        {
                            var msg = "Cancel order ref cannot be null or missing when the trade type is CANC.";
                            return msg;
                        }
                    }

                    //Trade Date
                    if (!requestParams.ContainsKey("Trade Date"))
                    {
                        errors.AppendLine("Trade date cannot be null or missing.");
                    }
                    else
                    {
                        if (DateTime.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "TradeDate").Value, out DateTime tempTo) == false)
                        {
                            errors.AppendLine($"Invalid format for trade date ({requestParams.FirstOrDefault(pair => pair.Key == "TradeDate").Value}), this must be yyyy-mm-dd and be a valid date");
                        }
                    }

                    //Settlement Amount
                    if (!requestParams.ContainsKey("Settlement Amount"))
                    {
                        errors.AppendLine("Settlement amount cannot be null or missing.");
                    }
                    else
                    {
                        if (!requestParams.FirstOrDefault(pair => pair.Key == "Settlement Amount").Value.ToString().All(char.IsNumber))
                        {
                            errors.AppendLine($"Settlement amount ({requestParams.FirstOrDefault(pair => pair.Key == "Settlement Amount").Value}) must contain numeric values only.");
                        }
                    }

                    //Instrument ISIN.
                    if (!requestParams.ContainsKey("Instrument ISIN"))
                    {
                        errors.AppendLine("Instrument ISIN cannot be null or missing.");
                    }

                    //Clearer Swift Code.
                    if (requestParams.ContainsKey("ClearerSwiftCode"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "ClearerSwiftCode").Value))
                        {
                            errors.AppendLine($"Clearer swift code ({requestParams.FirstOrDefault(pair => pair.Key == "ClearerSwiftCode").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Clearer swift code cannot be null or missing.");
                    }

                    //Agent Account Number.
                    if (!requestParams.ContainsKey("Agent Account Number"))
                    {
                        errors.AppendLine("Agent account number cannot be null or missing.");
                    }

                    //Settlement Swift Code.
                    if (requestParams.ContainsKey("Settlement Swift Code"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Settlement Swift Code").Value))
                        {
                            errors.AppendLine($"Settlement swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Settlement Swift Code").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Settlement swift code cannot be null or missing.");
                    }

                }
                #endregion
                #region MT543
                else if (requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value == "MT543")
                {
                    //Broker Swift Code.
                    if (requestParams.ContainsKey("BrokerSwiftCode"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "BrokerSwiftCode").Value))
                        {
                            errors.AppendLine($"Broker swift code ({requestParams.FirstOrDefault(pair => pair.Key == "BrokerSwiftCode").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Broker swift code cannot be null or missing.");
                    }

                    //Preparation Date.
                    if (!string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "PreparationDate").Value))
                    {
                        if (DateTime.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "PreparationDate").Value, out DateTime tempTo) == false)
                        {
                            errors.AppendLine($"Invalid format for Preperation Date ({requestParams.FirstOrDefault(pair => pair.Key == "PreparationDate").Value}), this must be yyyy-mm-dd and be a valid date");
                        }
                    }

                    //Trade Date.
                    if (!string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "TradeDate").Value))
                    {
                        if (DateTime.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "TradeDate").Value, out DateTime tempTo) == false)
                        {
                            errors.AppendLine($"Invalid format for Trade Date ({requestParams.FirstOrDefault(pair => pair.Key == "TradeDate").Value}), this must be yyyy-mm-dd and be a valid date");
                        }
                    }

                    //DealPrice - DealPerc
                    if (!requestParams.ContainsKey("DealPrice") &&
                        !requestParams.ContainsKey("DealPrice%"))
                    {
                        errors.AppendLine("Either a Deal Price or Deal Price% must be entered");
                    }

                    if (!string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "DealPrice").Value)
                        && string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "DealPrice%").Value))
                    {
                        double val;
                        bool parsed = double.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "DealPrice").Value, out val);
                        if (!parsed)
                        {
                            errors.AppendLine($"Deal Price ({requestParams.FirstOrDefault(pair => pair.Key == "DealPrice").Value}) must contain numeric values only.");
                        }
                    }
                    else if (string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "DealPrice").Value)
                        && !string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "DealPrice%").Value))
                    {
                        double valPerc;
                        bool parsedPerc = double.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "DealPrice%").Value, out valPerc);
                        if (!parsedPerc)
                        {
                            errors.AppendLine($"Deal Price% ({requestParams.FirstOrDefault(pair => pair.Key == "DealPrice").Value}) must contain numeric values only.");
                        }
                    }

                    //Quantity
                    if (!requestParams.ContainsKey("Quantity"))
                    {
                        errors.AppendLine("Quantity cannot be null or missing.");
                    }
                    else
                    {
                        if (!requestParams.FirstOrDefault(pair => pair.Key == "Quantity").Value.ToString().All(char.IsNumber))
                        {
                            errors.AppendLine($"Quantity ({requestParams.FirstOrDefault(pair => pair.Key == "Quantity").Value}) must contain numeric values only.");
                        }
                    }

                    //Buyer Swift Code.
                    if ((!requestParams.ContainsKey("BuyerSwiftCode") && !requestParams.ContainsKey("BuyerName")) &&
                        (!requestParams.ContainsKey("SellerSwiftCode") && !requestParams.ContainsKey("SellerName")))
                    {
                        errors.AppendLine("Either a Buyer swift code, Buyer Name or Seller Swift Code, Seller Name are required.");
                    }
                    else if (requestParams.ContainsKey("BuyerSwiftCode"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "BuyerSwiftCode").Value.Trim()))
                        {
                            errors.AppendLine($"Buyer swift code ({requestParams.FirstOrDefault(pair => pair.Key == "BuyerSwiftCode").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else if (requestParams.ContainsKey("SellerSwiftCode"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "SellerSwiftCode").Value.Trim()))
                        {
                            errors.AppendLine($"Seller swift code ({requestParams.FirstOrDefault(pair => pair.Key == "SellerSwiftCode").Value}) appears to be incorrectly formatted.");
                        }
                    }

                    //Custodian Swift Code.
                    if (!string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "CustodianSwiftCode").Value.Trim()))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "CustodianSwiftCode").Value.Trim()))
                        {
                            errors.AppendLine($"Custodian swift code ({requestParams.FirstOrDefault(pair => pair.Key == "CustodianSwiftCode").Value}) appears to be incorrectly formatted.");
                        }
                    }

                    //Previous Order Ref
                    if (requestParams.FirstOrDefault(pair => pair.Key == "TradeType").Value != "NEWM")
                    {
                        if (!requestParams.ContainsKey("PreviousOrderRef"))
                        {
                            var msg = "Previous  order ref cannot be null or missing when the trade type is CANC.";
                            return msg;
                        }
                    }

                    //Instrument ISIN.
                    if (!requestParams.ContainsKey("InstrumentISIN"))
                    {
                        errors.AppendLine("Instrument ISIN cannot be null or missing.");
                    }
                }
                #endregion
                #region MT545
                else if (requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value == "MT545")
                {

                    //Order Swift Code.
                    if (requestParams.ContainsKey("OrderSwiftCode"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "OrderSwiftCode").Value))
                        {
                            errors.AppendLine($"Order swift code ({requestParams.FirstOrDefault(pair => pair.Key == "OrderSwiftCode").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Order swift code cannot be null or missing.");
                    }

                    //Order Ref.
                    if (!requestParams.ContainsKey("OrderRef"))
                    {
                        errors.AppendLine("Order ref cannot be null or missing.");
                    }

                    //Related Ref.
                    if (!requestParams.ContainsKey("RelatedRef"))
                    {
                        errors.AppendLine("Related ref cannot be null or missing.");
                    }

                    //Trade Price.
                    if (!string.IsNullOrEmpty(requestParams.FirstOrDefault(pair => pair.Key == "TradePrice").Value))
                    {
                        double val;
                        bool parsed = double.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "TradePrice").Value, out val);
                        if (!parsed)
                        {
                            errors.AppendLine($"Trade price ({requestParams.FirstOrDefault(pair => pair.Key == "TradePrice").Value}) must contain numeric values only.");
                        }
                    }

                    //Trade Date
                    if (!requestParams.ContainsKey("TradeDate"))
                    {
                        errors.AppendLine("Trade date cannot be null or missing.");
                    }
                    else
                    {
                        if (DateTime.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "TradeDate").Value, out DateTime tempTo) == false)
                        {
                            errors.AppendLine($"Invalid format for trade date ({requestParams.FirstOrDefault(pair => pair.Key == "TradeDate").Value}), this must be yyyy-mm-dd and be a valid date");
                        }
                    }

                    //Instrument ISIN.
                    if (!requestParams.ContainsKey("InstrumentISIN"))
                    {
                        errors.AppendLine("Instrument ISIN cannot be null or missing.");
                    }

                    //Instrument Description.
                    if (!requestParams.ContainsKey("InstrumentDescription"))
                    {
                        errors.AppendLine("Instrument description cannot be null or missing.");
                    }

                    //Quantity
                    if (!requestParams.ContainsKey("Quantity"))
                    {
                        errors.AppendLine("Quantity cannot be null or missing.");
                    }
                    else
                    {
                        if (!requestParams.FirstOrDefault(pair => pair.Key == "Quantity").Value.ToString().All(char.IsNumber))
                        {
                            errors.AppendLine($"Quantity ({requestParams.FirstOrDefault(pair => pair.Key == "Quantity").Value}) must contain numeric values only.");
                        }
                    }

                    //Safekeeping Account Number.
                    if (!requestParams.ContainsKey("SafekeepingAccountNumber"))
                    {
                        errors.AppendLine("Safekeeping account number cannot be null or missing.");
                    }

                    //Agent Swift Code.
                    if (requestParams.ContainsKey("AgentSwiftCode"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "AgentSwiftCode").Value))
                        {
                            errors.AppendLine($"Agent swift code ({requestParams.FirstOrDefault(pair => pair.Key == "AgentSwiftCode").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Agent swift code cannot be null or missing.");
                    }

                    //Counter Party Swift Code.
                    if (requestParams.ContainsKey("Counterparty Swift Code"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Counterparty Swift Code").Value))
                        {
                            errors.AppendLine($"Counter party swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Counterparty Swift Code").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Counter party swift code cannot be null or missing.");
                    }

                    //Counter Party Settlement Swift Code.
                    if (requestParams.ContainsKey("Counterparty Settlement Swift Code"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Counterparty Settlement Swift Code").Value))
                        {
                            errors.AppendLine($"Counter party settlement swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Counterparty Settlement Swift Code").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Counterparty settlement swift code cannot be null or missing.");
                    }

                }
                #endregion
                #region MT547
                else if (requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value == "MT547")
                {
                    //Order Swift Code.
                    if (requestParams.ContainsKey("Order Swift Code"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value))
                        {
                            errors.AppendLine($"Order swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Order swift code cannot be null or missing.");
                    }

                    //Order Ref.
                    if (!requestParams.ContainsKey("Order Ref"))
                    {
                        errors.AppendLine("Order ref cannot be null or missing.");
                    }

                    //Related Ref.
                    if (!requestParams.ContainsKey("Related Ref"))
                    {
                        errors.AppendLine("Related ref cannot be null or missing.");
                    }

                    //Trade Date
                    if (!requestParams.ContainsKey("Trade Date"))
                    {
                        errors.AppendLine("Trade date cannot be null or missing.");
                    }
                    else
                    {
                        if (DateTime.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "Trade Date").Value, out DateTime tempTo) == false)
                        {
                            errors.AppendLine($"Invalid format for trade date ({requestParams.FirstOrDefault(pair => pair.Key == "Trade Date").Value}), this must be yyyy-mm-dd and be a valid date");
                        }
                    }

                    //Instrument ISIN.
                    if (!requestParams.ContainsKey("Instrument ISIN"))
                    {
                        errors.AppendLine("Instrument ISIN cannot be null or missing.");
                    }

                    //Instrument Description.
                    if (!requestParams.ContainsKey("Instrument Description"))
                    {
                        errors.AppendLine("Instrument description cannot be null or missing.");
                    }

                    //Quantity
                    if (!requestParams.ContainsKey("Quantity"))
                    {
                        errors.AppendLine($"Quantity cannot be null or missing.");
                    }
                    else
                    {
                        if (!requestParams.FirstOrDefault(pair => pair.Key == "Quantity").Value.ToString().All(char.IsNumber))
                        {
                            errors.AppendLine($"Quantity ({requestParams.FirstOrDefault(pair => pair.Key == "Quantity").Value}) must contain numeric values only.");
                        }
                    }

                    //Safekeeping Account Number.
                    if (!requestParams.ContainsKey("Safekeeping Account Number"))
                    {
                        errors.AppendLine("Safekeeping account number cannot be null or missing.");
                    }

                    //Broker Swift Code.
                    if (requestParams.ContainsKey("Broker Swift Code"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Broker Swift Code").Value))
                        {
                            errors.AppendLine($"Broker swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Broker Swift Code").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Broker swift code cannot be null or missing.");
                    }

                    //Counter Party Swift Code.
                    if (requestParams.ContainsKey("Counter Party Swift Code"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Counter Party Swift Code").Value))
                        {
                            errors.AppendLine($"Counter party swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Counter Party Swift Code").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Counter party swift code cannot be null or missing.");
                    }

                    //Counter Party Settlement Swift Code.
                    if (requestParams.ContainsKey("Counter Party Settlement Swift Code"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Counter Party Settlement Swift Code").Value))
                        {
                            errors.AppendLine($"Counter party settlement swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Counter Party Settlement Swift Code").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Counter party settlement swift code cannot be null or missing.");
                    }

                }
                #endregion
                #region MT565
                else if (requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value == "MT565")
                {
                    //Broker Swift Code.
                    if (requestParams.ContainsKey("BrokerSwiftCode"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "BrokerSwiftCode").Value))
                        {
                            errors.AppendLine($"Broker swift code ({requestParams.FirstOrDefault(pair => pair.Key == "BrokerSwiftCode").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Broker swift code cannot be null or missing.");
                    }

                    //Corporate Action Ref.
                    if (!requestParams.ContainsKey("CorporateActionRef"))
                    {
                        errors.AppendLine("Corporate action ref cannot be null or missing.");
                    }

                    //Link Fields.
                    if (requestParams.FirstOrDefault(pair => pair.Key == "TradeType").Value == "CANC")
                    {
                        if (!requestParams.ContainsKey("RelatedOrderRef"))
                        {
                            if (!requestParams.ContainsKey("RelatedOrderRef"))
                            {
                                errors.AppendLine("Related order ref cannot be null or missing when the trade type is CANC.");
                            }

                            if (!requestParams.ContainsKey("LinkIndicatorType"))
                            {
                                errors.AppendLine("Link indicator type cannot be null or missing when the trade type is CANC.");
                            }

                            if (!requestParams.ContainsKey("LinkMessageType"))
                            {
                                errors.AppendLine("Link message type cannot be null or missing when the trade type is CANC.");
                            }
                        }
                        else
                        {
                            if (!requestParams.ContainsKey("LinkIndicatorType"))
                            {
                                errors.AppendLine("Link indicator type cannot be null or missing when the trade type is CANC.");
                            }

                            if (!requestParams.ContainsKey("LinkMessageType"))
                            {
                                errors.AppendLine("Link message type cannot be null or missing when the trade type is CANC.");
                            }
                        }
                    }

                    //Instrument Quantity.
                    if (!requestParams.ContainsKey("InstrumentQty"))
                    {
                        errors.AppendLine($"Instrument quantity cannot be null or missing.");
                    }
                    else
                    {
                        if (!requestParams.FirstOrDefault(pair => pair.Key == "InstrumentQty").Value.ToString().All(char.IsNumber))
                        {
                            errors.AppendLine($"Instrument quantity ({requestParams.FirstOrDefault(pair => pair.Key == "InstrumentQty").Value}) must contain numeric values only.");
                        }
                    }

                    //Safekeeping Account.
                    if (!requestParams.ContainsKey("SafekeepingAccount"))
                    {
                        errors.AppendLine("Safekeeping account cannot be null or missing.");
                    }

                    //Beneficiary Swift Code.
                    if (requestParams.ContainsKey("BenficiarySwiftCode"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "BenficiarySwiftCode").Value))
                        {
                            errors.AppendLine($"Benficiary swift code ({requestParams.FirstOrDefault(pair => pair.Key == "BenficiarySwiftCode").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Benficiary swift code cannot be null or missing.");
                    }

                    //Corporate Action Number.
                    if (requestParams.FirstOrDefault(pair => pair.Key == "PreviousActionNotification").Value == "1")
                    {
                        if (!requestParams.ContainsKey("CorporateActionNumber"))
                        {
                            errors.AppendLine("Corporate action number cannot be null or missing when previous action notification is true");
                        }
                        else 
                        {
                            if (!requestParams.FirstOrDefault(pair => pair.Key == "CorporateActionNumber").Value.ToString().All(char.IsNumber))
                            {
                                errors.AppendLine($"Corporate action number ({requestParams.FirstOrDefault(pair => pair.Key == "CorporateActionNumber").Value}) must contain numeric values only.");
                            }
                            else
                            {
                                if (Convert.ToInt32(requestParams.FirstOrDefault(pair => pair.Key == "CorporateActionNumber").Value) > 999 ||
                                    Convert.ToInt32(requestParams.FirstOrDefault(pair => pair.Key == "CorporateActionNumber").Value) < 1)
                                {
                                    errors.AppendLine("Corporate action number must be between 001 and 999 only.");
                                }
                            }
                        }
                    }
                }
                #endregion
                #region MT548
                else if (requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value == "MT548")
                {
                    //Order Swift Code.
                    if (requestParams.ContainsKey("Order Swift Code"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value))
                        {
                            errors.AppendLine($"Order swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Order swift code cannot be null or missing.");
                    }

                    //Order Ref.
                    if (!requestParams.ContainsKey("Order Ref"))
                    {
                        errors.AppendLine("Order ref cannot be null or missing.");
                    }

                    //Trade Date
                    if (!requestParams.ContainsKey("Trade Date"))
                    {
                        errors.AppendLine("Trade date cannot be null or missing.");
                    }
                    else
                    {
                        if (DateTime.TryParse(requestParams.FirstOrDefault(pair => pair.Key == "Trade Date").Value, out DateTime tempTo) == false)
                        {
                            errors.AppendLine($"Invalid format for trade date ({requestParams.FirstOrDefault(pair => pair.Key == "Trade Date").Value}), this must be yyyy-mm-dd and be a valid date");
                        }
                    }

                    //Instrument ISIN.
                    if (!requestParams.ContainsKey("Instrument ISIN"))
                    {
                        errors.AppendLine("Instrument ISIN cannot be null or missing.");
                    }

                    //Instrument Description.
                    if (!requestParams.ContainsKey("Instrument Description"))
                    {
                        errors.AppendLine("Instrument description cannot be null or missing.");
                    }

                    //Quantity
                    if (!requestParams.ContainsKey("Quantity"))
                    {
                        errors.AppendLine($"Quantity cannot be null or missing.");
                    }
                    else
                    {
                        if (!requestParams.FirstOrDefault(pair => pair.Key == "Quantity").Value.ToString().All(char.IsNumber))
                        {
                            errors.AppendLine($"Quantity ({requestParams.FirstOrDefault(pair => pair.Key == "Quantity").Value}) must contain numeric values only.");
                        }
                    }

                    //Safekeeping Account Number.
                    if (!requestParams.ContainsKey("Safekeeping Account Number"))
                    {
                        errors.AppendLine("Safekeeping account number cannot be null or missing.");
                    }

                    //Agent Code.
                    if (requestParams.ContainsKey("Agent Code"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "AgentCode").Value))
                        {
                            errors.AppendLine($"Agent code ({requestParams.FirstOrDefault(pair => pair.Key == "AgentCode").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Agent code cannot be null or missing.");
                    }
                }
                #endregion
                #region MT920
                else if (requestParams.FirstOrDefault(pair => pair.Key == "SheetName").Value == "MT920")
                {
                    //Order Swift Code.
                    if (requestParams.ContainsKey("Order Swift Code"))
                    {
                        if (!genericFunctions.ValidSwift(requestParams.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value))
                        {
                            errors.AppendLine($"Order swift code ({requestParams.FirstOrDefault(pair => pair.Key == "Order Swift Code").Value}) appears to be incorrectly formatted.");
                        }
                    }
                    else
                    {
                        errors.AppendLine("Order swift code cannot be null or missing.");
                    }

                    //Order Ref.
                    if (!requestParams.ContainsKey("Order Ref"))
                    {
                        errors.AppendLine("Order ref cannot be null or missing.");
                    }

                    //Order Account Number.
                    if (!requestParams.ContainsKey("Order Account Number"))
                    {
                        errors.AppendLine("Order account number cannot be null or missing.");
                    }
                }
                    #endregion

                return errors.ToString();
            }
            catch (Exception ex)
            {
                _logger.Error("Error validating the Swift request received, error - ({})", ex.Message);
                return string.Empty;
            }
        }

        private bool ReturnBoolValue(int value)
        {
            if(value==1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected override void OnStop()
        {
            _logger.Info("SafeSearchService Stopping");
            _tmrExecuteProcess.Enabled = false;

        }
    }
}