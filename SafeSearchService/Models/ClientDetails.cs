﻿namespace SafeSearchService.Models
{
    public class ClientDetails
    {
        public string ClientId { get; set; }
        public string Name { set; get; }
        public string CompanyName { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ApiKey { get; set; }
        public bool Active { get; set; }
    }
}