﻿namespace SafeSearchService.Models
{
    public class MT548
    {
        public int ClientId { get; set; }
        public string ClientSwiftCode { get; set; }
        public string OrderSwiftCode { get; set; }
        public string OrderRef { get; set; }
        public string RelatedRef { get; set; }
        public string TradeDate { get; set; }
        public string SettleDate { get; set; }
        public string InstrumentISIN { get; set; }
        public string InstrumentDescription { get; set; }
        public string Ccy { get; set; }
        public string StatusCode { get; set; }
        public string ReasonCode { get; set; }
        public int? Quantity { get; set; }
        public double SettlementAmount { get; set; }
        public string SafekeepingAccount { get; set; }
        public string AgentAccountNumber { get; set; }
        public string AgentCode { get; set; }
        public bool SwiftUrgent { get; set; }
        public string Email { get; set; }
        public bool? Ftp { get; set; }
    }
}