﻿namespace SafeSearchService.Models
{
    public class Response
    {
        public string Message { get; set; }
        public int ResponseCode { get; set; }
    }
}
