﻿namespace SafeSearchService.Models
{
    public class MT210
    {
        public int ClientId { get; set; }
        public string ClientSwiftCode { get; set; }
        public string BenSwiftCode { get; set; }
        public string OrderRef { get; set; }
        public string BrokerAccountNo { get; set; }
        public string RelatedOrderRef { get; set; }
        public string SettleDate { get; set; }
        public double Amount { get; set; }
        public string Ccy { get; set; }
        public string OrderSwiftCode { get; set; }
        public string BenSubBankSwift { get; set; }
        public bool SwiftUrgent { get; set; }
        public string Email { get; set; }
        public bool? Ftp { get; set; }
    }
}
