﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SafeSearchService.Models
{
    public class MT399
    {
        public int ClientId { get; set; }
        public string ClientSwiftCode { get; set; }
        public string OrderSwiftCode { get; set; }
        public string OrderRef { get; set; }
        public string OrderRelatedRef { get; set; }
        public string Message { get; set; }
        public bool SwiftUrgent { get; set; }
        public string Email { get; set; }
        public bool? Ftp { get; set; }
    }
}