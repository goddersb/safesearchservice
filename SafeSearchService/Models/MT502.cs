﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SafeSearchService.Models
{
    public class MT502
    {
        public int ClientId { get; set; }
        public string ClientSwiftCode { get; set; }
        public string BrokerSwiftCode { get; set; }
        public string OrderRef { get; set; }
        public string BrokerAccountNumber { get; set; }
        public string TradeType { get; set; }
        public string CancelOrderRef { get; set; }
        public string BuySell { get; set; }
        public double Quantity { get; set; }
        public double Amount { get; set; }
        public string Ccy { get; set; }
        public string InstrumentISIN { get; set; }
        public string Email { get; set; }
        public bool? Ftp { get; set; }
    }
}
