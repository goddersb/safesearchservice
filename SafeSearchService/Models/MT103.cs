﻿using System;

namespace SafeSearchService.Models
{
    public class MT103
    {
        public int ClientId { get; set; }
        public string ClientSwiftCode { get; set; }
        public string SettleDate { get; set; }
        public double Amount { get; set; }
        public string Ccy { get; set; }
        public string OrderRef { get; set; }
        public string OrderName { get; set; }
        public string OrderAddress { get; set; }
        public string OrderCity { get; set; }
        public string OrderPostCode { get; set; }
        public string OrderSwift { get; set; }
        //public string OrderAccountNumber { get; set; }
        public string OrderIbanAccountNumber { get; set; }
        public string OrderOther { get; set; }
        public string OrderVOCode { get; set; }
    
        public string BeneficiaryName { get; set; }
        public string BeneficiaryAddress1 { get; set; }
        public string BeneficiaryAddress2 { get; set; }
        public string BeneficiaryPostCode { get; set; }
        public string BeneficiarySwift { get; set; }
        //public string BeneficiaryAccountNumber { get; set; }
        public string BeneficiaryIBAN { get; set; }
        public string BeneficiaryCcy { get; set; }
        public string BeneficiarySubBankName { get; set; }
        public string BeneficiarySubBankAddress1 { get; set; }
        public string BeneficiarySubBankAddress2 { get; set; }
        public string BeneficiarySubBankPostCode { get; set; }
        public string BeneficiarySubBankSwift { get; set; }
        public string BeneficiarySubBankIBAN { get; set; }
        //public string BeneficiarySubBankOther { get; set; }
        public string BeneficiarySubBankBik { get; set; }
        public string BeneficiarySubBankINN { get; set; }
        public string BeneficiaryBankName { get; set; }
        public string BeneficiaryBankAddress1 { get; set; }
        public string BeneficiaryBankAddress2 { get; set; }
        public string BeneficiaryBankPostCode { get; set; }
        public string BeneficiaryBankSwift { get; set; }
        public string BeneficiaryBankIBAN { get; set; }
        public bool SwiftUrgent { get; set; }
        public bool SwiftSendRef { get; set; }
        public double ExchangeRate { get; set; }
        public string Email { get; set; }
        public bool? Ftp { get; set; }

    }
}