﻿namespace SafeSearchService.Models
{
    public class SearchRequest
    {
        public int Threshold { get; set; }
        public bool? Pep { get; set; }
        public bool? PreviousSanctions { get; set; }
        public bool? CurrentSanctions { get; set; }
        public bool? LawEnforcement { get; set; }
        public bool? FinancialRegulator { get; set; }
        public bool? Insolvency { get; set; }
        public bool? DisqualifiedDirector { get; set; }
        public bool? AdverseMedia { get; set; }
        public string Forename { get; set; }
        public string Middlename { get; set; }
        public string Surname { get; set; }
        public string BusinessName { get; set; }
        public string DateOfBirth { get; set; }
        public string YearOfBirth { get; set; }
        public string DateOfDeath { get; set; }
        public string YearOfDeath { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string PostCode { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public bool? Ftp { get; set; }
    }
}
