﻿namespace SafeSearchService.Models
{
    public class MT540
    {
        public int ClientId { get; set; }
        public string ClientSwiftCode { get; set; }
        public string BrokerSwiftCode { get; set; }
        public string OrderRef { get; set; }
        public string BrokerAccountNumber { get; set; }
        public string TradeType { get; set; }
        public string CancelOrderRef { get; set; }
        public bool IsEquity { get; set; }
        public string TradeDate { get; set; }
        public string SettleDate { get; set; }
        public double Amount { get; set; }
        public string InstrumentISIN { get; set; }
        public string ClearerSwiftCode { get; set; }
        public string DeliveringAgentSwiftCode { get; set; }
        public string DeliveringAgent { get; set; }
        public string SettlementSwiftCode { get; set; }
        public string Instructions { get; set; }
        public string Email { get; set; }
        public bool? Ftp { get; set; }
    }
}
