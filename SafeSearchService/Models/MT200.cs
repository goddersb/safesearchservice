﻿namespace SafeSearchService.Models
{
    public class MT200
    {
        public int ClientId { get; set; }
        public string ClientSwiftCode { get; set; }
        public string BrokerSwiftCode { get; set; }
        public string OrderRef { get; set; }
        public string SettleDate { get; set; }
        public double Amount { get; set; }
        public string Ccy { get; set; }
        public string BenIBAN { get; set; }
        public string OrderIBAN { get; set; }
        public string OrderOther { get; set; }
        public bool SwiftUrgent { get; set; }
        public string Email { get; set; }
        public bool? Ftp { get; set; }
    }
}
