﻿namespace SafeSearchService.Models
{
    public class MT920
    {
        public int ClientId { get; set; }
        public string ClientSwiftCode { get; set; }
        public string OrderRef { get; set; }
        public string OrderSwift { get; set; }
        public string OrderCcy { get; set; }
        public double OrderAmount { get; set; }
        public string OrderAccountNumber { get; set; }
        public string OrderType { get; set; }
        public bool SwiftUrgent { get; set; }
        public string Email { get; set; }
        public bool? Ftp { get; set; }
    }
}
