﻿namespace SafeSearchService.Models
{
    public class MT542
    {
        public int ClientId { get; set; }
        public string ClientSwiftCode { get; set; }
        public string BrokerSwiftCode { get; set; }
        public string TradeType { get; set; }
        public string OrderRef { get; set; }
        public string CancelOrderRef { get; set; }
        public string TradeDate { get; set; }
        public string SettlementDate { get; set; }
        public bool IsEquity { get; set; }
        public string InstrumentISIN { get; set; }
        public double SettlementAmount { get; set; }
        public string BrokerAccountNo {get;set;}
        public string ClearerSwiftCode { get; set; }
        public string AgentAccountNo { get; set; }
        public string SettlementSwiftCode { get; set; }
        public string Instructions { get; set; }
        public string Email { get; set; }
        public bool? Ftp { get; set; }
    }
}