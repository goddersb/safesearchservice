﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SafeSearchService.Models
{
    public class MT380
    {
        public int ClientId { get; set; }
        public string ClientSwiftCode { get; set; }
        public string BrokerSwiftCode { get; set; }
        public string OrderRef { get; set; }
        public string PreviousOrderRef { get; set; }
        public string LinkMessageType { get; set; }
        public string TradeType { get; set; }
        public string SafeKeepingAccount { get; set; }
        public string BuySell { get; set; }
        public string TradeDate { get; set; }
        public string SettleDate { get; set; }
        public double Amount { get; set; }
        public string OrderCcy { get; set; }
        public string CounterCcy { get; set; }
        public string Message { get; set; }
        public string Email { get; set; }
        public bool? Ftp { get; set; }
    }
}
