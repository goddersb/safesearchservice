﻿namespace SafeSearchService.Models
{
    public class FilePaths
    {
        public string PathName { get; set; }
        public string PathFilePath { get; set; }
    }
}
