﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SafeSearchService.Models
{
    public class MT565
    {
        public int ClientId { get; set; }
        public string ClientSwiftCode { get; set; }
        public string OrderSwiftCode { get; set; }
        public string OrderRef { get; set; }
        public string CorporateActionRef { get; set; }
        public string TradeType { get; set; }
        public string CorporateActionEventIndicator { get; set; }
        public string RelatedOrderRef { get; set; }
        public string LinkIndicatorType { get; set; }
        public string LinkMessageType { get; set; }
        public string InstrumentISIN { get; set; }
        public double InstrumentQty { get; set; }
        public string InstrumentQtyType { get; set; }
        public string SafekeepingAccount { get; set; }
        public string BeneficiarySwiftCode { get; set; }
        public string CorporateActionNumber { get; set; }
        public string CorporateActionIndicator { get; set; }
        public string Email { get; set; }
        public bool PreviousActionNotification { get; set; }
        public bool? Ftp { get; set; }
    }
}