﻿namespace SafeSearchService.Models
{
    public class MT541
    {
        public int ClientId { get; set; }
        public string ClientSwiftCode { get; set; }
        public string BrokerSwiftCode { get; set; }
        public string OrderRef { get; set; }
        public string PreviousOrderRef { get; set; }
        public string LinkedMessageType { get; set; }
        public bool SwiftUrgent { get; set; }
        public string TradeType { get; set; }
        public string PreparationDate { get; set; }
        public string TradeDate { get; set; }
        public string SettlementDate { get; set; }
        public double? DealPrice { get; set; }
        public double? DealPerc { get; set; }
        public string DealCcy { get; set; }
        public string InstrumentISIN { get; set; }
        public double Quantity { get; set; }
        public string SafeKeepingAccountType { get; set; }
        public string SafeKeepingAccount { get; set; }
        public string SafeKeepingPlace { get; set; }
        public string CustodianSwiftCode { get; set; }
        public string OrderInstrumentType { get; set; }
        public string SellerSwiftCode { get; set; }
        public string SellerName { get; set; }
        public string SellerAddress { get; set; }
        public string BuyerSwiftCode { get; set; }
        public string BuyerName { get; set; }
        public string BuyerAddress{ get; set; }
        public string SettlementSwiftCode { get; set; }
        public string SettlementTransactionType { get; set; }
        public string SettlementTransactionIndicator { get; set; }
        public string SettlementPartyType { get; set; }
        public string SettlementPartyName { get; set; }
        public string DeliveryAgentCode { get; set; }
        public string SettlementAmountType { get; set; }
        public double SettlementAmount { get; set; }
        public string SettlementCcy { get; set; }
        public string Email { get; set; }
        public bool? Ftp { get; set; }
    }
}
