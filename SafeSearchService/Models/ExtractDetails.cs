﻿namespace SafeSearchService.Models
{
    public class ExtractDetails
    {
        public int Index { get; set; }
        public string ClientId { get; set; }
        public string ExtractPath { get; set; }
        public string SearchType { get; set; }
        public string EmailId { get; set; }
    }
}
